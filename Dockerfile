FROM node as react-build

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY . ./
RUN npm rebuild node-sass
# RUN npm install
# RUN npm build

# start app
CMD ["npm", "run", "start"]