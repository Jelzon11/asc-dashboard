import {
  FETCH_COMPANY_REPORTS,
  FETCH_COMPANY_PRODUCTS_REPORTS,
  FETCH_PRODUCT_MATERIALS_REPORTS,
  FETCH_MATERIAL_STREAMRECORDS_REPORTS,
  FETCH_VIOLATORS_BY_COMPANY_REPORTS,
  FETCH_VIOLATORS_BY_PRODUCT_REPORTS,
  FETCH_MAINCATEGORIES_BY_VIOLATIONS_STATUS,
  FETCH_STREAM_RECORD_STATUS,
  FETCH_VIOLATIONS_REPORTS,
  FETCH_NON_EXISTING_MATERIAL,
  FETCH_ME,
  RESET_COMPANY_PRODUCTS_REPORTS,
  FETCH_ALL
} from "../actions/types";

const initialState = {
  //selectedDataRange: {},
  //brandViolationsCount: [],
  //productViolationCount: [],
  //materialViolationCount: [],
  //materialStreamRecords: [],

  companyReports: [],
  companyProductReport: [],
  productMaterialsReport: [],
  materialStreamRecordsReport: [],
  nonExistingMaterials: [],
  me: [],
  allData: [],

  //////////////////////////////////
  violationsCauseReport: [],
  companyViolators: [],
  productViolators: [],
  mainCategoryViolationStatus: [],
  streamRecordsStatus: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_COMPANY_REPORTS:
      return {
        ...state,
        companyReports: action.payload
      };
    case FETCH_COMPANY_PRODUCTS_REPORTS:
      return {
        ...state,
        companyProductReport: action.payload
      };
    case RESET_COMPANY_PRODUCTS_REPORTS:
      return {
        ...state,
        companyProductReport: []
      };
    case FETCH_PRODUCT_MATERIALS_REPORTS:
      return {
        ...state,
        productMaterialsReport: action.payload
      };
    case FETCH_MATERIAL_STREAMRECORDS_REPORTS:
      return {
        ...state,
        materialStreamRecordsReport: action.payload
      };

    //////////////////////////////////////
    case FETCH_VIOLATORS_BY_COMPANY_REPORTS:
      return {
        ...state,
        companyViolators: action.payload
      };

    case FETCH_VIOLATORS_BY_PRODUCT_REPORTS:
      return {
        ...state,
        productViolators: action.payload
      };

    case FETCH_VIOLATIONS_REPORTS:
      return {
        ...state,
        violationsCauseReport: action.payload
      };

    case FETCH_MAINCATEGORIES_BY_VIOLATIONS_STATUS:
      return {
        ...state,
        mainCategoryViolationStatus: action.payload
      };
    case FETCH_STREAM_RECORD_STATUS:
      return {
        ...state,
        streamRecordsStatus: action.payload
      };

    case FETCH_NON_EXISTING_MATERIAL:
      return {
        ...state,
        nonExistingMaterials: action.payload
      };
    case FETCH_ME:
      return {
        ...state,
        me: action.payload
      };
      case FETCH_ALL:
      return {
        ...state,
        allData: action.payload
      };
    default:
      return state;
  }
}
