import React, { Component } from "react";
import ContentHolder from "./components/pages/ContentHolder";
import './App.css';
import { Provider } from "react-redux";
import store from "./store";
import Login from "../src/components/pages/Login";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Provider store={store}>
              <ContentHolder/>
            </Provider>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
