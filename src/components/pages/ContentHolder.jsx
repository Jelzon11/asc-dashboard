import React, { Fragment, useState } from "react";
import { Route } from "react-router-dom";
import { Switch } from 'react-router-dom';
import { connect } from "react-redux";

//Component
import Dashboard from "./Dashboard";
import Error404 from "./Error404";
import Reports from "./Reports";
import CompanyReport from "./CompanyReport";
import ProductReport from "./ProductReport";
import RadioMaterialReport from "./RadioMaterialReport"; 
import OnlineMaterialReport from "./OnlineMaterialReport";
import TopNavigation from "./TopNavigation";
import SideNavigation from "./SideNavigation";
import NonExistingMaterialReport from './NonExistingMaterialReport';
import MasterList from './MasterList';
import Footer from "../Footer";
import { 
  fetchCompanyReportData,
  fetchMyInfo,
  fetchCompanyProductReportData,
  //fetchNonExistingMaterials,
  //fetchAllData
} from '../../actions/companyAction';
import { decrypted } from "../utils";
let isFirstLoad = false;

const ContentHolder = props => {
  const [isAuthenticed, setAuthentication] = useState(false);
  if(!isFirstLoad){
    decrypted();
    props.fetchCompanyReportData();
    props.fetchMyInfo(({isAuthenticated}) => {
      if(!isAuthenticated) {
        localStorage.removeItem('ascarchiving')
        window.location = '/';
      } else {
        setAuthentication(true)
      }
    })
    isFirstLoad = true;
  }

  return isAuthenticed && (
    <Fragment>
        <TopNavigation />
        <SideNavigation />
        <Switch>
          <Route exact path="/dashboard" component={Dashboard} />
          <Route exact path="/companyreports" component={Reports} />
          <Route exact path="/companyreports/:companyId" component={CompanyReport} />
          <Route exact path="/companyreports/:companyId/productreports/:productId" component={ProductReport} />
          <Route exact path="/companyreports/:companyId/productreports/:productId/materialreports/:materialId" component={RadioMaterialReport}/>
          <Route exact path="/onlineMaterialReport" component={OnlineMaterialReport} />
          <Route exact path="/masterList" component={MasterList}/>
          <Route exact path="/nonExistingMaterial" component={NonExistingMaterialReport} />
          <Route component={Error404} />
        </Switch>
        <Footer />
    </Fragment>
  )
};

export default connect(null, {
  fetchCompanyReportData,
  fetchCompanyProductReportData,
  fetchMyInfo,
  //fetchNonExistingMaterials,
  //fetchAllData
})(ContentHolder);

