import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { MDBListGroup, MDBListGroupItem, MDBIcon } from "mdbreact";
import { NavLink } from "react-router-dom";
import ascIMG from "../images/asc1.png";
import avatarIMG from "../images/avatar.png";
import { isAuthorize } from "../utils";

//Components
const _dashboardNavigation = '898f394e-5458-4c38-9200-4581e877ff6e';
const _matchUnMatchNavigation = '29750ac7-8d48-47b0-b2c2-9c3e3fe904d4';
const _nonExistingMaterialNavigation = '78be1104-8d54-4f4b-8d21-47a7c6ce6071';
const _masterlistStreamMaterialNavigation = '75c41020-5de0-4043-bb40-2964fd044dff';


const SideNavigation = props => {
  const [myInfo, setMyInfo] = useState("");

  useEffect(() => {
    setMyInfo(props.me);
  }, [props.me]);

  return (
    <div
      style={{ backgroundColor: "#" }}
      className="main-sidebar sidebar-light-danger elevation-4"
    >
      <center>
        <a href="#!" className="logo-wrapper waves-effect">
          <img alt="ASC Logo" width="75%" className="img-fluid" src={ascIMG} />
        </a>
      </center>
      <hr />
      <div className="sidebar">
        <MDBListGroup
          className="list-group-flush"
          style={{ fontSize: ".9rem" }}
        >
          <MDBListGroupItem>
            <center>
              <img
                src={avatarIMG}
                className="rounded mx-auto d-block"
                alt="aligment"
                width="100"
                height="100"
              />
              <a href="#" className="d-block" style={{ fontSize: "1rem" }}>
                {myInfo.firstName} {myInfo.lastName}
              </a>
              <text style={{ color: "#3283a8", fontSize: "0.8rem" }}>
                {myInfo && myInfo.roleDetails && myInfo.roleDetails.name}
              </text>
            </center>
          </MDBListGroupItem>
          {isAuthorize(_dashboardNavigation) && (
            <NavLink exact={true} to="/dashboard" activeClassName="activeClass">
              <MDBListGroupItem>
                <MDBIcon icon="chart-pie" className="mr-3" />
                Dashboard
              </MDBListGroupItem>
            </NavLink>
          )}
          {isAuthorize(_matchUnMatchNavigation) && <NavLink to="/companyreports" activeClassName="activeClass">
            <MDBListGroupItem>
              <MDBIcon icon="table" className="mr-3" />
              Match & Unmatch
            </MDBListGroupItem>
          </NavLink>
          }
          {isAuthorize(_nonExistingMaterialNavigation) &&
            <NavLink to="/nonExistingMaterial" activeClassName="activeClass">
              <MDBListGroupItem>
                <MDBIcon icon="table" className="mr-3" />
              Non-ASC Materials
            </MDBListGroupItem>
            </NavLink>
          }
          {isAuthorize(_masterlistStreamMaterialNavigation) &&
            <NavLink to="/masterList" activeClassName="activeClass">
              <MDBListGroupItem>
                <MDBIcon icon="archive" className="mr-3" />
              Stream Masterlist
            </MDBListGroupItem>
            </NavLink>
          }
        </MDBListGroup>
      </div>
    </div>
  );
};
const mapStateToProps = state => ({
  me: state.data.me
});

export default connect(mapStateToProps)(SideNavigation);
