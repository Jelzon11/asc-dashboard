import React, { useReducer, useEffect, useState } from "react";
import { DateRangeInput } from "@datepicker-react/styled";
import { ThemeProvider } from "styled-components";
import { MDBRow, MDBCol, MDBBtn } from "mdbreact";
import moment from "moment";
import { connect } from "react-redux";
import { fetchAllData } from "../../actions/companyAction";

const initialState = {
  startDate: null,
  endDate: null,
  focusedInput: null
};

function reducer(state, action) {
  switch (action.type) {
    case "focusChange":
      return { ...state, focusedInput: action.payload };
    case "dateChange":
      return action.payload;
    default:
      throw new Error();
  }
}

const DateRangePickerMasterlist = props => {
  const [state, dispatch] = useReducer(reducer, initialState);
  

  useEffect(() => {
    if(state && state.startDate && state.endDate){
      const startDateMoment = moment(state.startDate).format("YYYY-MM-DD");
      const endDateMoment = moment(state.endDate).format("YYYY-MM-DD");
      props.fetchAllData(startDateMoment, endDateMoment);
    }      
  }, [state.startDate && state.endDate]);

  return (
    <ThemeProvider
      theme={{
        breakpoints: ["32em", "50em", "64em"],
        reactDatepicker: {
          daySize: [36, 40],
          fontFamily: "system-ui, -apple-system",
          zIndex: "9",
          colors: {
            accessibility: "#D80249",
            selectedDay: "#f7518b",
            selectedDayHover: "#F75D95",
            primaryColor: "#d8366f"
          }
        }
      }}
    >
      <DateRangeInput
        onDatesChange={data => {
          dispatch({ type: "dateChange", payload: data });
        }}
        onFocusChange={focusedInput => {
          dispatch({ type: "focusChange", payload: focusedInput })
        }}
        startDate={state.startDate}
        endDate={state.endDate}
        focusedInput={state.focusedInput}
      />
    </ThemeProvider>
  );
};

export default connect(null, {
  fetchAllData
})(DateRangePickerMasterlist);
