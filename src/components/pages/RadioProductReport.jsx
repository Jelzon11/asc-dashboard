import React, { Fragment, useState, useEffect } from "react";
import { Link, withRouter } from "react-router-dom";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBDataTable,
  MDBCardText,
  MDBIcon,
  MDBBtn,
  MDBBreadcrumb,
  MDBBreadcrumbItem
} from "mdbreact";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
import { commercialCategories } from "../utils";
const _companyProductMaterialStreamRecordReportPage = '338d8533-cb2e-4f5d-bb1b-15f81f59aa47';

const columns = () => {
  return [
    {
      label: "Material name",
      field: "name",
      sort: "asc",
      width: 150
    },
    {
      label: "Commercial Type",
      field: "commercialType",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Compliant",
      field: "totalComplied",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Non-Compliant",
      field: "totalNonComplied",
      sort: "asc",
      width: 270
    },
    {
      label: "Potential Non-Compliant",
      field: "totalPotentialNonCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Original Material",
      field: "originalMaterial",
      sort: "asc",
      width: 270
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 270
    }
  ];
};

const RadioProductReport = props => {
  const [radioMaterialList, setMaterialList] = useState([]);
  const { productId, companyId } = props.match.params;

  useEffect(() => {
    props.materialList && setMaterialList(props.materialList);
  }, [props.materialList]);

  const renderData = radioMaterialList => {
    const newMaterialList = radioMaterialList.map(radioMaterial => {
      return {
        name: radioMaterial.title,
        commercialType: commercialCategories.filter(
          ct => ct.id == radioMaterial.commercialType
        )[0].name,
        totalComplied: (
          <MDBCardText style={{ color: "green", fontWeight: "bold" }}>
            {radioMaterial.compliedCount}
          </MDBCardText>
        ),
        totalNonComplied: (
          <MDBCardText style={{ color: "red", fontWeight: "bold" }}>
            {radioMaterial.confirmedCount}
          </MDBCardText>
        ),
        totalPotentialNonCompliance: (
          <MDBCardText style={{ color: "orange", fontWeight: "bold" }}>
            {radioMaterial.nonCompliedCount}
          </MDBCardText>
        ),
        originalMaterial: (
          <Fragment>
            <audio
              src={radioMaterial.fileURL}
              controls
              style={{ width: "100%" }}
            ></audio>
          </Fragment>
        ),
        action: (
          <Fragment>
            <center>
              <Link
                to={{
                  pathname: `/companyreports/${companyId}/productreports/${productId}/materialreports/${radioMaterial.id}`
                }}
              >
                <MDBBtn color="light-blue" size="sm">
                  <MDBIcon icon="eye" size="1x" className="eye-button" />
                </MDBBtn>
              </Link>
            </center>
          </Fragment>
        )
      };
    });
    return newMaterialList;
  };

  return isAuthorize(_companyProductMaterialStreamRecordReportPage) ? (
    <MDBCard className="mb-5">
      <MDBCardHeader tag="h4" className="text-uppercase">
        Radio Materials
      </MDBCardHeader>
      <MDBCardBody className="d-flex align-items-center justify-content-between">
        <MDBDataTable
          striped
          fixed={true}
          small
          bordered
          hover
          order={["name", "desc"]}
          disableRetreatAfterSorting={true}
          sortable={true}
          responsive={true}
          searchLabel={"Search..."}
          responsiveSm={true}
          responsiveLg={true}
          responsiveXl={true}
          noBottomColumns={true}
          data={{
            rows:
              radioMaterialList.length >= 1
                ? renderData(radioMaterialList)
                : [],
            columns: columns()
          }}
        />
      </MDBCardBody>
    </MDBCard>
  ) : <UnAuthorize />;
};

export default connect()(withRouter(RadioProductReport));
