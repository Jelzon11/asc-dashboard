import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBCardText,
  MDBDataTable,
  MDBIcon,
  MDBBtn
} from "mdbreact";
import {
  fetchCompanyProductReportData,
  resetSelectedCompanyProductReportData
} from "../../actions/companyAction";
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
const _companyProductReportPage = 'c56715a2-7f29-4a40-b161-eeadbb443316'

const columns = () => {
  return [
    {
      label: "Brand name",
      field: "name",
      sort: "asc",
      width: 150
    },
    {
      label: "No of Materials",
      field: "noOfMaterials",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Compliant",
      field: "totalComplied",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Non-Compliant",
      field: "totalNonComplied",
      sort: "asc",
      width: 270
    },
    {
      label: "Potential Non-Compliant",
      field: "totalPotentialNonCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 270
    }
  ];
};

const CompanyReport = props => {
  const [companyData, setCompanyData] = useState({});
  const [productList, setProductList] = useState([]);

  const { companyId } = props.match.params;

  if (Object.keys(companyData).length <= 0)
    props.fetchCompanyProductReportData(companyId);
  if (props.companyProductReport && props.companyProductReport.productsData) {
    setProductList(props.companyProductReport.productsData);
    delete props.companyProductReport["productsData"];
    setCompanyData(props.companyProductReport);
  }

  const renderData = productList => {
    const newProductList = productList.map(product => {
      return {
        name: product.name,
        noOfMaterials: product.materialcount,
        totalComplied: (
          <MDBCardText style={{ color: "green", fontWeight: "bold" }}>
            {product.compliedCount}
          </MDBCardText>
        ),
        totalNonComplied: (
          <MDBCardText style={{ color: "red", fontWeight: "bold" }}>
            {product.confirmedCount}
          </MDBCardText>
        ),
        totalPotentialNonCompliance: (
          <MDBCardText style={{ color: "orange", fontWeight: "bold" }}>
            {product.nonCompliedCount}
          </MDBCardText>
        ),
        action: (
          <Fragment>
            <center>
              <Link
                to={{
                  pathname: `/companyreports/${companyData.id}/productreports/${product.id}`
                }}
              >
                <MDBBtn color="light-blue" size="sm">
                  <MDBIcon icon="eye" size="1x" className="eye-button" />
                  
                </MDBBtn>
              </Link>
            </center>
          </Fragment>
        )
      };
    });
    return newProductList;
  };

  return isAuthorize(_companyProductReportPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBBreadcrumb className={'breadCrumb'}>
              <MDBBreadcrumbItem>
                <Link  
                  className={'breadCrumb-link'} 
                  to={{ pathname: "/companyreports" }} active
                >
                  COMPANIES
                </Link>
              </MDBBreadcrumbItem>
              <MDBBreadcrumbItem>
                <Link
                  className={'breadCrumb-link'}
                  to={{
                    pathname: `/companyreports/${companyData && companyData.id}`
                  }}
                >
                  {companyData && companyData.name}
                </Link>
              </MDBBreadcrumbItem>
            </MDBBreadcrumb>
            <br />
            <MDBCard className="mb-5">
              <MDBCardHeader>
                <h3>{companyData && companyData.name} Report</h3>
              </MDBCardHeader>
              <MDBCardBody>
                <h5>
                  Total Non-Compliant:{" "}
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "30px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "red"
                    }}
                  >
                    {companyData && companyData.nonCompliant}
                  </text>
                </h5>
                <h5>
                  Total Potential Non-Compliant:{" "}
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "30px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "orange"
                    }}
                  >
                    {companyData && companyData.potentialNonCompliance}
                  </text>
                </h5>
                <br />
                <MDBCard className="mb-5">
                  <MDBCardHeader
                    tag="h4"
                    className="text-uppercase"
                  >
                    Products
                  </MDBCardHeader>
                  <MDBCardBody className="d-flex align-items-center justify-content-between">
                    <MDBDataTable
                      striped
                      fixed={true}
                      small
                      bordered
                      hover
                      order={["name", "desc"]}
                      disableRetreatAfterSorting={true}
                      sortable={true}
                      responsive={true}
                      searchLabel={"Search..."}
                      responsiveSm={true}
                      responsiveLg={true}
                      responsiveXl={true}
                      noBottomColumns={true}
                      data={{
                        rows:
                          productList.length >= 1
                            ? renderData(productList)
                            : [],
                        columns: columns()
                      }}
                    />
                  </MDBCardBody>
                </MDBCard>
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};

CompanyReport.propTypes = {
  fetchCompanyProductReportData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  companyProductReport: state.data.companyProductReport
});

export default connect(mapStateToProps, {
  fetchCompanyProductReportData,
  resetSelectedCompanyProductReportData
})(CompanyReport);
