import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";
import "toastr/build/toastr.min";
import "../../../../node_modules/toastr/build/toastr.css";
import toastr from "toastr";
import { violations, setToInvalid, setToValid } from "../../utils";
import { Select } from "../Utility-Component";
import {
  updateStatus,
  fetchMaterialStreamRecordsReportData,
  fetchCompanyReportData,
  fetchStreamRecordsStatus,
  fetchCompanyProductReportData,
  fetchProductMaterialsReportData,
  fetchProductViolatorsData
} from "../../../actions/companyAction";

const ViewFailModal = props => {
  const [selectedStreamData, setSelectedStreamData] = useState({});
  const [violation, selectedViolation] = useState(0);
  const [violationList, setViolationList] = useState(0);
  const [reason, setReason] = useState("");

  useEffect(() => {
    setSelectedStreamData(props.selectedStreamData);
    //setMaterialFullDetails(props.materialFullDetails);
    setViolationList(violations.filter(violation => violation.id != 1));
  }, [props]);

  const closeSelfHandler = () => {
    props.handleFailModal();
    selectedViolation(0);
    setReason("");
  };
  
  const violationOnChangeHandler = e => {
    e.target.value !== 0 ? setToValid(e.target) : setToInvalid(e.target);
    const violation = e.target.value;
    selectedViolation(violation);
  };
  const reasonOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const reason = e.target.value;
    setReason(reason);
  };

  const handleSubmit = () => {
    let companyId = selectedStreamData.companyId;
    let materialId = selectedStreamData.materialId;
    let productId = selectedStreamData.productId;
    const statusData = {
      streamRecordId: selectedStreamData && selectedStreamData.id,
      violationCause: violation,
      reason: reason,
      isComplied: false,
      isConfirmed: true
    };
    props.updateStatus(statusData, () => {
      props.fetchMaterialStreamRecordsReportData(materialId, () => { });
      props.fetchCompanyProductReportData(companyId, () => { });
      props.fetchProductMaterialsReportData(productId, () => { });
      props.fetchCompanyReportData();
      props.fetchStreamRecordsStatus();
      props.fetchProductViolatorsData();
      toastr.success("Successfully evaluated", "System");
      closeSelfHandler();
      props.closeViewModalHandler();
    });
  };
  return (
    <MDBModal
      size="md"
      isOpen={props.isFailModalOpen}
      toggle={props.handleFailModal}
      position="top"
      backdrop={false}
      modalStyle="danger"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Fail</MDBModalHeader>
      <form>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label> Violation:</label>
              <Select
                value={violationList}
                items={violationList}
                onChange={e => violationOnChangeHandler(e)}
                name={"violationSelect"}
                key={"violationSelect"}
                required
              />
              <div className="invalid-feedback">Please Select Violation</div>
            </MDBCol>
          </MDBRow>
          <br />
          <MDBRow>
            <MDBCol>
              <label> Reason:</label>
              <textarea
                onChange={e => reasonOnChangeHandler(e)}
                className="form-control rounded-1"
                rows="3"
              ></textarea>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <div>
            <MDBBtn color="light-green" onClick={() => handleSubmit()}>
              <font style={{ color: "white" }}>Submit </font>
            </MDBBtn>
          </div>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
ViewFailModal.propTypes = {
  updateStatus: PropTypes.func.isRequired
};

export default connect(null, {
  updateStatus,
  fetchMaterialStreamRecordsReportData,
  fetchCompanyReportData,
  fetchStreamRecordsStatus,
  fetchCompanyProductReportData,
  fetchProductMaterialsReportData,
  fetchProductViolatorsData
})(withRouter(ViewFailModal));
