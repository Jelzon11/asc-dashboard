import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";
import "toastr/build/toastr.min";
import "../../../../node_modules/toastr/build/toastr.css";
import toastr from "toastr";
import { setToInvalid, setToValid } from "../../utils";
import {
  updateStatus,
  fetchMaterialStreamRecordsReportData,
  fetchCompanyReportData,
  fetchStreamRecordsStatus,
  fetchCompanyProductReportData,
  fetchProductMaterialsReportData,
  fetchProductViolatorsData
} from "../../../actions/companyAction";

const ViewPassModal = props => {
  const [selectedStreamData, setSelectedStreamData] = useState({});
  const [reason, setReason] = useState("");

  useEffect(() => {
    setSelectedStreamData(props.selectedStreamData);
    //setMaterialFullDetails(props.materialFullDetails);
  }, [props]);

  const closeSelfHandler = () => {
    props.handlePassModal();
    setReason("");
  };

  const reasonOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const reason = e.target.value;
    setReason(reason);
  };
  
  const handleSubmit = () => {
    let companyId = selectedStreamData.companyId;
    let materialId = selectedStreamData.materialId;
    let productId = selectedStreamData.productId;

    const statusData = {
      streamRecordId: selectedStreamData && selectedStreamData.id,
      violationcause: 1,
      reason: reason,
      isComplied: true,
      isConfirmed: false
    };
    
    props.updateStatus(statusData, () => {
      props.fetchMaterialStreamRecordsReportData(materialId, () => { });
      props.fetchCompanyProductReportData(companyId, () => { });
      props.fetchProductMaterialsReportData(productId, () => { });
      props.fetchCompanyReportData();
      props.fetchStreamRecordsStatus();
      props.fetchProductViolatorsData();
      toastr.success("Successfully evaluated", "System");
      closeSelfHandler();
      props.closeViewModalHandler();
    });
  };
  return (
    <MDBModal
      size="md"
      isOpen={props.isPassModalOpen}
      toggle={props.handlePassModal}
      full-height
      position="top"
      backdrop={false}
      modalStyle="info"
    >
      <MDBModalHeader toggle={closeSelfHandler}>Pass</MDBModalHeader>
      <form>
        <MDBModalBody>
          <MDBRow>
            <MDBCol>
              <label> Reason:</label>
              <textarea
                onChange={e => reasonOnChangeHandler(e)}
                className="form-control rounded-1"
                rows="3"
              ></textarea>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <div>
            <MDBBtn color="light-green" onClick={() => handleSubmit()}>
              <font style={{ color: "white" }}>Submit </font>
            </MDBBtn>
          </div>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
ViewPassModal.propTypes = {
  updateStatus: PropTypes.func.isRequired
};

export default connect(null, {
  updateStatus,
  fetchMaterialStreamRecordsReportData,
  fetchCompanyReportData,
  fetchStreamRecordsStatus,
  fetchCompanyProductReportData,
  fetchProductMaterialsReportData,
  fetchProductViolatorsData
})(withRouter(ViewPassModal));
