import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";
import "../../../../node_modules/toastr/build/toastr.min.js";
import "../../../../node_modules/toastr/build/toastr.css";
import toastr from "toastr";
import { violations, commercialCategories, mediums } from "../../utils";
import {
  updateStatus,
  fetchMaterialStreamRecordsReportData,
  fetchCompanyReportData,
  fetchStreamRecordsStatus,
  fetchCompanyProductReportData,
  fetchProductMaterialsReportData,
  fetchProductViolatorsData
} from "../../../actions/companyAction";
import ViewFailModal from "./viewFailModal.jsx";
import ViewPassModal from './viewPassModal';

const ViewModalRadioMaterial = props => {
  const [materialFullDetails, setMaterialFullDetails] = useState({});
  const [selectedStreamData, setSelectedStreamData] = useState({});
  const { companyId, productId, materialId } = props.match.params;

  const [isFailModalOpen, setFailModalOpen] = useState(false);
  const [isPassModalOpen, setPassModalOpen] = useState(false);

  useEffect(() => {
    setSelectedStreamData(props.selectedRecordStream);
    setMaterialFullDetails(props.materialFullDetails);
  }, [props]);

  const closeViewModalHandler = () => {
    props.handleViewModal();
  };

  const handleFailModal = () => {
    if (isFailModalOpen == false) {
      //const streamRecord = selectedStreamData.find(item => item.id == streamId);
      //setSelectecSteamRecord(streamRecord);
      setFailModalOpen(true);
    } else {
      setFailModalOpen(false);
    }
  };

  const handlePassModal = () => {
    if (isPassModalOpen == false) {
      //const streamRecord = selectedStreamData.find(item => item.id == streamId);
      //setSelectecSteamRecord(streamRecord);
      setPassModalOpen(true);
    } else {
      setPassModalOpen(false);
    }
  };

  // const handleSubmit = (isComplied) => {
  //   const statusData = {
  //     streamRecordId: selectedStreamData && selectedStreamData.id,
  //     isComplied: isComplied.isComplied,
  //     isConfirmed: isComplied.isConfirmed
  //   };
  //   props.updateStatus(statusData, () => {
  //     props.fetchMaterialStreamRecordsReportData(materialId, () => {});
  //     props.fetchCompanyProductReportData(companyId, () => {});
  //     props.fetchProductMaterialsReportData(productId, () => {});
  //     props.fetchCompanyReportData();
  //     props.fetchStreamRecordsStatus();
  //     props.fetchProductViolatorsData();
  //     toastr.success("Successfully evaluated", "System");
  //     closeViewModalHandler();
  //   });
  // };

  return (
    <MDBModal
      size="lg"
      isOpen={props.isModalOpen}
      toggle={props.handleViewModal}
      full-height
      position="top"
    >
      <ViewFailModal
        isFailModalOpen={isFailModalOpen}
        handleFailModal={handleFailModal}
        selectedStreamData={selectedStreamData}
        closeViewModalHandler={closeViewModalHandler}
        //materialFullDetails={materialFullDetails}
      />

      <ViewPassModal
        isPassModalOpen={isPassModalOpen}
        handlePassModal={handlePassModal}
        selectedStreamData={selectedStreamData}
        closeViewModalHandler={closeViewModalHandler}
        //materialFullDetails={materialFullDetails}
      />
      <MDBModalHeader toggle={closeViewModalHandler}>
        {materialFullDetails && materialFullDetails.title}
      </MDBModalHeader>
      <form>
        <MDBModalBody>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Reference Code:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {materialFullDetails && materialFullDetails.referenceId}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Media Type:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {materialFullDetails &&
                  materialFullDetails.mediumId &&
                  mediums.filter(
                    medium => medium.id == materialFullDetails.mediumId
                  )[0].type}{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Commercial Type:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {materialFullDetails &&
                  materialFullDetails.commercialType &&
                  commercialCategories.filter(
                    ct => ct.id == materialFullDetails.commercialType
                  )[0].name}{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Station:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {selectedStreamData && selectedStreamData.channel}{" "}
              </label>
            </MDBCol>
          </MDBRow>

          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Region:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {selectedStreamData && selectedStreamData.region}{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                City:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {selectedStreamData && selectedStreamData.city}{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Status:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {selectedStreamData && selectedStreamData.status}{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Violation:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {selectedStreamData &&
                  selectedStreamData.violationCause &&
                  violations.filter(
                    violation =>
                      violation.id == selectedStreamData.violationCause
                  )[0].name}{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Reason:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {selectedStreamData && selectedStreamData.reason}
              </label>
            </MDBCol>
          </MDBRow>
          <hr />
          <MDBRow>
            <MDBCol className="d-flex justify-content-center">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Evidence:{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-center">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Original Material{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-center">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Aired Material{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-center">
              <audio
                src={materialFullDetails && materialFullDetails.fileURL}
                controls
              ></audio>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-center">
              <audio
                src={selectedStreamData && selectedStreamData.fileURL}
                controls
              ></audio>
            </MDBCol>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          {selectedStreamData &&
          selectedStreamData.status === "Potential-Non-Compliant" ? (
            <div>
              <MDBBtn
                color="red"
                onClick={() =>
                  handleFailModal(selectedStreamData)
                }
              >
                <font style={{color: "white"}}>Fail </font>
              </MDBBtn>
              <MDBBtn
                color="light-green"
                onClick={() =>
                  handlePassModal(selectedStreamData)
                }
              >
                <font style={{color: "white"}}>Pass </font>
              </MDBBtn>
            </div>
          ) : (
            <MDBBtn color="grey" onClick={closeViewModalHandler}>
              Close
            </MDBBtn>
          )}
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
ViewModalRadioMaterial.propTypes = {
  updateStatus: PropTypes.func.isRequired
};

export default connect(null, {
  updateStatus,
  fetchMaterialStreamRecordsReportData,
  fetchCompanyReportData,
  fetchStreamRecordsStatus,
  fetchCompanyProductReportData,
  fetchProductMaterialsReportData,
  fetchProductViolatorsData
})(withRouter(ViewModalRadioMaterial));
