import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBBtn,
  MDBModalFooter,
  MDBRow,
  MDBCol
} from "mdbreact";
import "../../../../node_modules/toastr/build/toastr.min.js";
import "../../../../node_modules/toastr/build/toastr.css";
import toastr from "toastr";
import {
  updateReason,
  fetchNonExistingMaterials
} from "../../../actions/companyAction";


const ViewModalNonExisting = props => {
  const [nonExistingRecord, setNonExistingRecord] = useState({});
  const [remarksToggle, setRemarksToggle] = useState(false);
  const [reason, setReason] = useState("");

  const reasonOnChangeHandler = e => {
    const reason = e.target.value;
    setReason(reason);
  };
  const toggleAddRemarks = () => {
    setRemarksToggle(true)
  } 
  useEffect(() => {
    setNonExistingRecord(props.nonExistingRecord);
  }, [props.nonExistingRecord]);

  const closeSelfHandler = () => {
    props.handleViewModal();
    setRemarksToggle(false);
  };
//console.log(nonExistingRecord);

const handleSubmit = () => {
  const nonExistingMaterialStatus = {
    notInTheArchiveId: nonExistingRecord && nonExistingRecord.id,
    reason: reason
  };
  //console.log(nonExistingMaterialStatus);
  props.updateReason(nonExistingMaterialStatus, () => {
    props.fetchNonExistingMaterials();
    toastr.success("Successfully evaluated", "System");
    closeSelfHandler();
  });
};
  return (
    <MDBModal
      size="lg"
      isOpen={props.isModalOpen}
      toggle={props.handleViewModal}
      full-height
      position="top"
    >
      <MDBModalHeader toggle={closeSelfHandler}>
       {nonExistingRecord && nonExistingRecord.productName} 
      </MDBModalHeader>
      <form>
        <MDBModalBody>
        <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Main Category:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {nonExistingRecord && nonExistingRecord.mainCategory}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Sub Category:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {nonExistingRecord && nonExistingRecord.subCategory}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Commercial Type:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {/* {materialFullDetails &&
                  materialFullDetails.commercialType &&
                  commercialCategories.filter(
                    ct => ct.id == materialFullDetails.commercialType
                  )[0].name}{" "} */}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Station:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {nonExistingRecord && nonExistingRecord.channel}{" "} 
              </label>
            </MDBCol>
          </MDBRow>

          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Region:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                 {nonExistingRecord && nonExistingRecord.region}{" "} 
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                City:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {nonExistingRecord && nonExistingRecord.city}{" "} 
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Violation:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                Non-Existing Material
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol size="6" className="d-flex justify-content-end">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Remarks:{" "}
              </label>
            </MDBCol>
            <MDBCol size="6" className="d-flex justify-content-start">
              <label style={{ fontWeight: "normal", fontSize: "20px" }}>
                {" "}
                {nonExistingRecord && nonExistingRecord.reason}
              </label>
            </MDBCol>
          </MDBRow>
          <hr />
          <MDBRow>
            <MDBCol className="d-flex justify-content-center">
              <label style={{ fontWeight: "lighter", fontSize: "20px" }}>
                Aired Material:{" "}
              </label>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <MDBCol className="d-flex justify-content-center">
              <audio
                src={nonExistingRecord && nonExistingRecord.fileURL}
                controls
              ></audio>
            </MDBCol>
          </MDBRow>
          { remarksToggle == false ?(<MDBRow>
            <MDBCol className="d-flex justify-content-center">
            <MDBBtn color="grey" color="light-green" onClick={() => {toggleAddRemarks()}}>
          <font style={{color: "white"}}>Add remarks</font>
          </MDBBtn>
            </MDBCol>
          </MDBRow>) : (
            <div>
          <MDBRow>
            <MDBCol className="d-flex justify-content-center">
              <textarea onChange={e => reasonOnChangeHandler(e)} name="remarks" id="" cols="40" rows="5"></textarea>
              </MDBCol>
              </MDBRow>
              <MDBRow>
              <MDBCol className="d-flex justify-content-center">
            <MDBBtn color="grey" color="light-green" onClick={() => handleSubmit()}>
          <font style={{color: "white"}}>Save</font>
          </MDBBtn>
          </MDBCol>
           
          </MDBRow>
          </div>)}
          
          {/*  */}
        </MDBModalBody>
        <MDBModalFooter>
        <MDBBtn color="grey" onClick={closeSelfHandler}>
        <font style={{color: "white"}}> Close </font>
            </MDBBtn>
        </MDBModalFooter>
      </form>
    </MDBModal>
  );
};
ViewModalNonExisting.propTypes = {
  updateReason: PropTypes.func.isRequired
};

export default connect(null, {
  updateReason,
  fetchNonExistingMaterials
})(ViewModalNonExisting);

