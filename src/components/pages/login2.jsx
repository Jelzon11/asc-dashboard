import React, {} from 'react';
import { Link } from 'react-router-dom';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBCardText } from 'mdbreact';

const Login = () => {
    return (
       
        <MDBContainer>
        <MDBRow>
          <MDBCol style={{ height: "3rem" }}></MDBCol>
        </MDBRow>
        <MDBRow>
        <MDBCol lg="3" sm="2" />
          <MDBCol lg="6" sm="8">
          
            <MDBCard style={{backgroundImage: "linear-gradient(#F5515F, #A1051D)"}}>
              <MDBCardBody style={{color:"white"}}>
              <div style={{ textAlign: "center", marginTop: "1rem" }}>
                <img
                  src=""
                  className="img-circle"
                ></img>
              </div>
                <form>
                  <p className="h1 text-center py-4">ASC Logo here</p>
                  <div>
                  <label>Email: </label>
                    <MDBInput  style={{color:"white"}}
                      //label="Your email"
                      icon="envelope"
                      group
                      type="email"
                      validate
                      error="wrong"
                      success="right"
                    />
                    <label>Password: </label>
                    <MDBInput
                    style={{color:"white"}}
                      //label="Your password"
                      icon="lock"
                      group
                      type="password"
                      validate
                    />
                  </div>
                  <div className="text-center py-4 mt-3">
                    <MDBBtn gradient="red" type="submit" style={{borderRadius: "40px", width: "200px", height: "50px"}}>
                      Login
                    </MDBBtn>
                    
                  </div>
                  <div className="text-center">
                 <Link to='#'>
                 <MDBCardText style={{color: "white"}}>Forget password?</MDBCardText>
                 </Link>
                    
                  </div>
                  
                </form>
              </MDBCardBody>
            </MDBCard>
            </MDBCol>
          <MDBCol lg="3" sm="2" />
        </MDBRow>
      </MDBContainer>
   
    )
}

export default Login;