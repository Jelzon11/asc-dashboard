import React, { useState, useEffect, Fragment, useReducer } from "react";
import { DateRangeInput } from "@datepicker-react/styled";
import { ThemeProvider } from "styled-components";
import { Link } from "react-router-dom";
import {
  MDBCard,
  MDBCardBody,
  MDBDataTable,
  MDBCardHeader,
  MDBBtn,
  MDBIcon,
  MDBCol,
  MDBRow
} from "mdbreact";
import { connect } from "react-redux";
import moment from "moment-timezone";
import DateRangePickerMasterlist from "./DateRangePickerMasterlist";
import { fetchAllData } from "../../actions/companyAction";
import { generateReport } from '../utils';
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
const _masterlistStreamMaterialPage = '9855c48c-4aac-4ba3-915a-1942dac7e048'


const columns = () => {
  return [
    {
      label: "Material",
      field: "material",
      sort: "asc",
      width: "10%",
      fontSize: ".7rem"
    },
    {
      label: "Station",
      field: "station",
      sort: "asc",
      width: "10%"
    },
    {
      label: "City",
      field: "city",
      sort: "asc",
      width: "10%"
    },
    {
      label: "Company",
      field: "company",
      sort: "asc",
      width: 300
    },
    {
      label: "Product",
      field: "product",
      sort: "asc",
      width: "10%"
    },
    {
      label: "Main Category",
      field: "mainCategory",
      sort: "asc",
      width: "10%"
    },
    {
      label: "Sub-Category",
      field: "subCategory",
      sort: "asc",
      width: "10%"
    },
    {
      label: "Date Aired",
      field: "dateAired",
      sort: "asc",
      width: 300
    },
    {
      label: "Status",
      field: "status",
      sort: "asc",
      width: "10%"
    },
    {
      label: "Aired Material",
      field: "airedMaterial",
      sort: "asc",
      width: 300
    }
  ];
};

const reportColumn = [{
  key: 'id',
  header: 'Stream ID'
}, {
  key: 'referenceId',
  header: 'Reference ID'
}, {
  key: 'duration',
  header: 'Duration'
}, {
  key: 'channel',
  header: 'Channel'
}, {
  key: 'city',
  header: 'City'
}, {
  key: 'region',
  header: 'Region'
}, {
  key: 'companyDetails',
  subKey: 'name',
  header: 'Company'
}, {
  key: 'productDetails',
  subKey: 'name',
  header: 'Product'
}, {
  key: 'mainCategoryDetails',
  subKey: 'name',
  header: 'Category'
}, {
  key: 'startRecordTimestamp',
  header: 'DateTime'
}];

const MasterList = props => {
  const [masterListData, setMasterListData] = useState([]);
  const [streamRecordList, setStreamRecordList] = useState([]);

  if (Object.keys(masterListData).length <= 0) {
    props.fetchAllData();
  }

  const generateXMLReport = () => {
    generateReport({
      column: reportColumn,
      data: streamRecordList,
      subject: 'ASC Streams List',
      workBookTitle: 'sheet'
    });
  }

  if (props.masterlist && props.masterlist.streamRecordList) {
    setStreamRecordList(props.masterlist.streamRecordList);
    delete props.masterlist['streamRecordList'];
    setMasterListData(props.masterlist);
  }

  useEffect(() => {

  }, [props.masterlist]);

  const renderData = streamRecordList => {
    const newmasterListData = streamRecordList.map(data => {
      return {
        material: (
          <Link
            className="hover"
            to={{ pathname: `/companyreports/${data.companyId}/productreports/${data.productId}/materialreports/${data.materialId}` }}
          >
            {data.materialDetails.title}
          </Link>
        ),
        station: data.channel,
        city: data.city,
        company: (
          <Link className="hover"
            to={{
              pathname: `/companyreports/${data.companyId}`
            }}
          >
            {data.companyDetails.name}
          </Link>
        ),
        product: (
          <Link className="hover"
            to={{
              pathname: `/companyreports/${data.companyId}/productreports/${data.productId}`
            }}
          >
            {data.productDetails.name}
          </Link>
        ),
        mainCategory: data.mainCategoryDetails.name,
        subCategory: data.subCategoryDetails.name,
        dateAired: moment.unix(data.startRecordTimestamp).format("MM/DD/YYYY hh:mm:ss a"),
        status: data.status == 'Potential-Non-Compliant'
          ? <text style={{ color: 'rgb(248, 148, 6' }}>{data.status}</text>
          : data.status == 'Compliant'
            ? <text style={{ color: 'rgb(92, 181, 76' }}>{data.status}</text>
            : <text style={{ color: 'rgb(199, 50, 40' }}>{data.status}</text>
        ,
        airedMaterial: (
          <Fragment>
            <audio
              src={data.fileURL}
              controls
            ></audio>
          </Fragment>
        )
      };
    });
    return newmasterListData;
  };
  return isAuthorize(_masterlistStreamMaterialPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBCard className="mb-5">
              <MDBCardHeader>
                <MDBRow>
                  <MDBCol size="7">
                    <h1>MasterList</h1>
                  </MDBCol>
                  <MDBCol size="5">
                    <DateRangePickerMasterlist />
                  </MDBCol>
                </MDBRow>
                <MDBCol size="5"></MDBCol>
                <MDBCol>
                  <MDBBtn onClick={() => generateXMLReport()}>XLSX Report</MDBBtn>
                </MDBCol>
              </MDBCardHeader>
              <MDBCardBody className="d-flex align-items-center justify-content-between">
                <MDBDataTable
                  striped
                  fixed={true}
                  small
                  bordered
                  hover
                  order={["name", "desc"]}
                  disableRetreatAfterSorting={true}
                  sortable={true}
                  responsive={true}
                  searchLabel={"Search..."}
                  responsiveSm={true}
                  responsiveLg={true}
                  responsiveXl={true}
                  noBottomColumns={true}
                  data={{
                    rows: (streamRecordList && streamRecordList.length) >= 1 ? renderData(streamRecordList) : [],
                    columns: columns()
                  }}
                />
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};

const mapStateToProps = state => ({
  masterlist: state.data.allData
});
export default connect(mapStateToProps, { fetchAllData })(MasterList);
