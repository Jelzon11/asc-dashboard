import "../css/datatables.css";
import "../css/bs4.css";
import React, { Fragment, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCol,
  MDBRow,
  MDBDataTable,
  MDBIcon,
  MDBBtn
} from "mdbreact";
import { Doughnut, Pie } from "react-chartjs-2";
import data from "../utils/listOfPlatforms";

const columns = () => {
  return [
    {
      label: "Material name",
      field: "name",
      sort: "asc",
      width: 150
    },
    {
      label: "Commercial Type",
      field: "commercialType",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Compliance",
      field: "totalCompliance",
      sort: "asc",
      width: 270
    },

    {
      label: "Total Non-Compliance",
      field: "totalNonCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Original Material",
      field: "originalMaterial",
      sort: "asc",
      width: 270
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 270
    }
  ];
};



const RadioProductReport = (props) => {

  const [productDetails, setProductDetails] = useState({});
  const [onlineMaterialList, setOnlineMaterialList] = useState([]);

  useEffect(() => {
    //setProductDetails(props.location.state);
    setOnlineMaterialList(data);
  }, []);


  const history = useHistory();

  const renderData = onlineMaterialList => {
    //console.log(radioMaterialList);
    const newOnlineMaterialList = onlineMaterialList.map(onlineMaterial => {
      return {
        name: onlineMaterial.name,
        totalCompliance: onlineMaterial.totalCompliance,
        commercialType: onlineMaterial.commercialType,
        totalNonCompliance: onlineMaterial.totalNonCompliance,
        originalMaterial: onlineMaterial.originalMaterial,
        action: (
          <Fragment>
            <center>
              <MDBBtn
                onClick={() => {
                  history.push("/onlineMaterialReport", { onlineMaterial: onlineMaterial });
                }}
                color="light-blue"
                size="sm"
              >
                <MDBIcon icon="eye" size="2x" color="white" />
              </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newOnlineMaterialList;
  };

  return (
    <div>
      <p className="mt-2" style={{ fontWeight: "500", fontSize: "1.5rem", color: "#eb6b34" }}>Total Violation: 219</p>
      <MDBCard className="mb-5">
        <MDBCardHeader
          tag="h4"
          className="font-weight-bold text-uppercase"
        >
          Unofficial table
                      </MDBCardHeader>
        <MDBCardBody className="d-flex align-items-center justify-content-between">
          <MDBDataTable
            striped
            fixed={true}
            small
            bordered
            hover
            order={["name", "desc"]}
            disableRetreatAfterSorting={true}
            sortable={true}
            responsive={true}
            searchLabel={"Search..."}
            responsiveSm={true}
            responsiveLg={true}
            responsiveXl={true}
            noBottomColumns={true}
            data={{
              rows: renderData(onlineMaterialList),
              columns: columns()
            }}
          />
        </MDBCardBody>
      </MDBCard>
    </div>
  );
};

export default RadioProductReport;
