import React, { useState, useEffect, isValidElement, Fragment } from "react";
import { useHistory, Link, Redirect } from "react-router-dom";
import {
  MDBInput,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBBtn,
  MDBCardBody
} from "mdbreact";
import {
  getToken,
  isValidToken,
  removeToken,
  setToInvalid,
  setToValid,
  isTokenExist
} from "../utils";
import { PageSpinner } from "./Utility-Component";

const Login = () => {
  let history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isAuthenticated, setAuthentication] = useState(false);
  const [invalidAccessMessage, setInvalidAccessMessage] = useState("");
  const [isInProgress, setToInProgress] = useState(false);

  const emailOnChangeHandler = e => {
    setEmail(e.target.value);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const passwordOnChangeHandler = e => {
    setPassword(e.target.value);
    e.target.value ? setToValid(e.target) : setToInvalid(e.target);
  };

  const loginRedirect = () => {
    window.location = "/dashboard";
  };

  const submitHanlder = e => {
    e.preventDefault();
    if (!email) setToInvalid(e.target.elements["email"]);
    if (!password) setToInvalid(e.target.elements["password"]);
    if (!email || !password) return false;
    setToInProgress(true);
    fetch(process.env.REACT_APP_URL + "/api/users/logins", {
      method: "POST",
      headers: { "content-type": "application/json" },
      body: JSON.stringify({
        email,
        password
      })
    })
      .then(async res => {
        if (res.status >= 400) {
          const resJson = await res.json();
          throw new Error(resJson.message);
        }
        return res.json();
      })
      .then(resData => JSON.stringify(resData))
      .then(str => window.btoa(str))
      .then(encoded => localStorage.setItem("ascarchiving", encoded))
      .then(() => {
        loginRedirect();
      })
      .catch(err => {
        if (
          err.message == "Invalid input Password" ||
          err.message == "Email not found"
        ) {
          setInvalidAccessMessage(err.message);
        } else {
          setInvalidAccessMessage("Unable to login");
        }
        setToInProgress(false);
      });
  };

  return !isTokenExist() ? (
    isInProgress == true ? (
      <PageSpinner />
    ) : (
      <MDBContainer>
        <MDBRow>
          <MDBCol style={{ height: "3rem" }}></MDBCol>
        </MDBRow>
        <MDBRow>
          <MDBCol lg="3" sm="2" />
          <MDBCol lg="6" sm="8">
            <MDBCard
              style={{ backgroundImage: "linear-gradient(#ffff, #ffff)" }}
            >
              <MDBCardBody style={{ color: "red" }}>
                <div style={{ textAlign: "center", marginTop: "1rem" }}>
                <a href="#!" className="logo-wrapper waves-effect">
        <img alt="ASC Logo" width="75%" className="img-fluid" src="../asc1.png" />
      </a>
                </div>
                <hr />
                {invalidAccessMessage && (
                  <div
                    className="alert alert-danger"
                    role="alert"
                    style={{ margin: ".5rem" }}
                  >
                    {invalidAccessMessage}
                  </div>
                )}
                <div style={{ padding: "1rem" }}>
                  <form
                    onSubmit={e => submitHanlder(e)}
                    className="needs-validation"
                    noValidate
                  >
                    <div className="form-group">
                      <MDBInput
                        style={{ color: "red" }}
                        onChange={e => {
                          emailOnChangeHandler(e);
                        }}
                        icon="envelope"
                        group
                        type="email"
                        validate
                        error="wrong"
                        success="right"
                        name={"email"}
                        required
                      >
                        <div
                          style={{ color: "red" }}
                          className="invalid-feedback"
                        >
                          Please provide Email
                        </div>
                      </MDBInput>
                    </div>
                    <div className="form-group">
                      <MDBInput
                        style={{ color: "red" }}
                        onChange={e => {
                          passwordOnChangeHandler(e);
                        }}
                        icon="lock"
                        group
                        validate
                        type="password"
                        name={"password"}
                        required
                      >
                        <div
                          style={{ color: "red" }}
                          className="invalid-feedback"
                        >
                          Please provide Password
                        </div>
                      </MDBInput>
                    </div>
                    <div className="text-center py-4 mt-3">
                      <MDBBtn
                        gradient="red"
                        type="submit"
                        style={{
                          borderRadius: "40px",
                          width: "200px",
                          height: "50px"
                        }}
                      >
                        Login
                      </MDBBtn>
                    </div>
                    {/* <div className="form-group">
                    <div className="clearfix">
                     
                      <div className="float-right">
                        <MDBBtn color="light-green" type="submit">
                            Submit
                        </MDBBtn>
                      </div>
                    </div>
                  </div> */}
                  </form>
                </div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="3" sm="2" />
        </MDBRow>
      </MDBContainer>
    )
  ) : (
    loginRedirect()
  );
};

export default Login;
