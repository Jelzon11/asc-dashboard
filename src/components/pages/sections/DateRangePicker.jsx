import React, { useReducer, useEffect, useState } from "react";
import { DateRangeInput } from "@datepicker-react/styled";
import { ThemeProvider } from "styled-components";
import { MDBRow, MDBCol, MDBBtn } from "mdbreact";
import moment from "moment";
import { connect } from "react-redux";
import {
  fetchCompanyViolatorsData,
  fetchMainCategoriesViolationStatus,
  fetchStreamRecordsStatus,
  fetchProductViolatorsData,
  fetchViolationCauseData
} from "../../../actions/companyAction";

const initialState = {
  startDate: null,
  endDate: null,
  focusedInput: null
};

function reducer(state, action) {
  switch (action.type) {
    case "focusChange":
      return { ...state, focusedInput: action.payload };
    case "dateChange":
      return action.payload;
    default:
      throw new Error();
  }
}

const DateRangePicker = props => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const today = moment().format("YYYY-MM-DD");
  const last7Days = moment()
    .subtract(7, "d")
    .format("YYYY-MM-DD");
  const last30Days = moment()
    .subtract(30, "d")
    .format("YYYY-MM-DD");


  useEffect(() => {
    const startDateMoment = moment(state.startDate).format("YYYY-MM-DD");
    const endDateMoment = moment(state.endDate).format("YYYY-MM-DD");
    let dateRange = {
      startdate: startDateMoment,
      enddate: endDateMoment
    };
    state.startDate &&
      state.endDate &&
      props.fetchStreamRecordsStatus(dateRange);
    state.startDate &&
      state.endDate &&
      props.fetchMainCategoriesViolationStatus(dateRange);
    state.startDate &&
      state.endDate &&
      props.fetchViolationCauseData(dateRange);
    state.startDate &&
      state.endDate &&
      props.fetchProductViolatorsData(dateRange);
    state.startDate &&
      state.endDate &&
      props.fetchCompanyViolatorsData(dateRange);
  }, [state.startDate && state.endDate]);

  const handleTodayButton = () => {
    const startDateMoment = today;
    const endDateMoment = today;
    let dateRange = {
      startdate: startDateMoment,
      enddate: endDateMoment
    };
    props.fetchStreamRecordsStatus(dateRange);
    props.fetchMainCategoriesViolationStatus(dateRange);
    props.fetchViolationCauseData(dateRange);
    props.fetchProductViolatorsData(dateRange);
    props.fetchCompanyViolatorsData(dateRange);
  };
  const handleLast7DaysButton = () => {
    const startDateMoment = last7Days;
    const endDateMoment = today;
    let dateRange = {
      startdate: startDateMoment,
      enddate: endDateMoment
    };
    props.fetchStreamRecordsStatus(dateRange);
    props.fetchMainCategoriesViolationStatus(dateRange);
    props.fetchViolationCauseData(dateRange);
    props.fetchProductViolatorsData(dateRange);
    props.fetchCompanyViolatorsData(dateRange);
  };

  const handleLast30DaysButton = () => {
    const startDateMoment = last30Days;
    const endDateMoment = today;
    let dateRange = {
      startdate: startDateMoment,
      enddate: endDateMoment
    };
    props.fetchStreamRecordsStatus(dateRange);
    props.fetchMainCategoriesViolationStatus(dateRange);
    props.fetchViolationCauseData(dateRange);
    props.fetchProductViolatorsData(dateRange);
    props.fetchCompanyViolatorsData(dateRange);
  };

  const handleReset = () => {
    props.fetchStreamRecordsStatus();
    props.fetchMainCategoriesViolationStatus();
    props.fetchViolationCauseData();
    props.fetchProductViolatorsData();
    props.fetchCompanyViolatorsData();
  };
  return (
    <MDBRow>
      <MDBCol size="5">
        <ThemeProvider
          theme={{
            breakpoints: ["32em", "48em", "64em"],
            reactDatepicker: {
              daySize: [36, 40],
              fontFamily: "system-ui, -apple-system",
              zIndex: "9",
              colors: {
                accessibility: "#D80249",
                selectedDay: "#f7518b",
                selectedDayHover: "#F75D95",
                primaryColor: "#d8366f"
              }
            }
          }}
        >
          <DateRangeInput
            onDatesChange={data => {
              dispatch({ type: "dateChange", payload: data });
              if (data.endDate && data.startDate) {
              } else {
                props.fetchStreamRecordsStatus();
                props.fetchMainCategoriesViolationStatus();
                props.fetchViolationCauseData();
                props.fetchProductViolatorsData();
                props.fetchCompanyViolatorsData();
              }
            }}
            onFocusChange={focusedInput =>
              dispatch({ type: "focusChange", payload: focusedInput })
            }
            startDate={state.startDate}
            endDate={state.endDate}
            focusedInput={state.focusedInput}
          />
        </ThemeProvider>
      </MDBCol>

      <MDBCol size="7">
        <div className="pull-right">
          <MDBRow className="d-flex justify-content-end">
            <MDBBtn
              size="sm"
              color="blue-grey"
              onClick={() => handleTodayButton()}
            >
              <font style={{color: "white"}}>Today</font>
            </MDBBtn>
            <MDBBtn
              size="sm"
              onClick={() => handleLast7DaysButton()}
              color="blue-grey"
            >
              <font style={{color: "white"}}>Last Week</font>
            </MDBBtn>
            <MDBBtn
              size="sm"
              onClick={() => handleLast30DaysButton()}
              color="blue-grey"
            >
              <font style={{color: "white"}}>Last 30 days</font>
            </MDBBtn>
            <MDBBtn size="sm" onClick={() => handleReset()} color="blue-grey">
              <i class="fas fa-sync pr-2" aria-hidden="true"></i>
              <font style={{color: "white"}}>Reset</font>
            </MDBBtn>

            {/* <MDBBtn size="sm" color="blue-grey">
              Last Year
            </MDBBtn> */}
          </MDBRow>
        </div>
      </MDBCol>
    </MDBRow>
  );
};

export default connect(null, {
  fetchCompanyViolatorsData,
  fetchMainCategoriesViolationStatus,
  fetchStreamRecordsStatus,
  fetchProductViolatorsData,
  fetchViolationCauseData
})(DateRangePicker);
