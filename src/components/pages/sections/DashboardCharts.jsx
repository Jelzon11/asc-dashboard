import React, { useEffect, useState } from "react";
import {
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBRow,
  MDBTable,
  MDBTableHead,
  MDBTableBody
} from "mdbreact";
import { Pie, Doughnut, HorizontalBar } from "react-chartjs-2";
import { connect } from "react-redux";
import { subString, violations } from "../../utils";


const DashboardCharts = props => {
  const [mainCategoryDataSet, setMainCategorySet] = useState({});
  const [streamRecordStatusDataSet, setstreamRecordStatusDataSet] = useState(
    {}
  );
  const [productViolators, setProductViolators] = useState({});
  const [violationCauses, setViolationCauses] = useState({});
  let productViolatorsToList = [];
  let violationCausesToList = [];

  const setMainCategoryData = mainCategoryStatusData => {
    const labels = mainCategoryStatusData.map(
      item => item.mainCategoryDetails.name
    );
    const nonComplianceData = mainCategoryStatusData.map(item =>
      item.noncompliedcount ? item.noncompliedcount : 0
    );
    const potentialNonComplianceData = mainCategoryStatusData.map(item =>
      item.potentialnoncompliedcount ? item.potentialnoncompliedcount : 0
    );
    const nonComliancesDataSet = {
      data: nonComplianceData,
      label: "Non Compliant",
      backgroundColor: "rgba(158, 39, 16, .7)",
      borderColor: "rgba(158, 39, 16, 1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(158, 39, 16, .7)",
      hoverBorderColor: "rgba(158, 39, 16, 1)"
    };
    const PotentialNonComliancesDataSet = {
      data: potentialNonComplianceData,
      label: "Potential Non Compliant",
      backgroundColor: "rgba(248, 148, 6, .7)",
      borderColor: "rgba(248, 148, 6, 1)",
      borderWidth: 1,
      hoverBackgroundColor: "rgba(248, 148, 6, .7)",
      hoverBorderColor: "rgba(248, 148, 6, 1)"
    };
    return {
      labels,
      datasets: [PotentialNonComliancesDataSet, nonComliancesDataSet]
    };
  };

  const setStreamRecordStatus = streamRecordsStatus => {
    return {
      labels: ["Compliant", "Non-Compliant", "Potential-Non-Compliant"],

      datasets: [
        {
          data: [
            streamRecordsStatus.compliance,
            streamRecordsStatus.nonComplience,
            streamRecordsStatus.potentialNonComplience
          ],
          backgroundColor: [
            "rgba(92, 181, 76, .9)",
            "rgba(158, 39, 16, .9",
            "rgba(248, 148, 6, .9)",
          ]
        }
      ]
    };
  };

  useEffect(() => {
    if (props.mainCategoryViolationStatus.mainCategories)
      setMainCategorySet(
        setMainCategoryData(props.mainCategoryViolationStatus.mainCategories)
      );
    if (props.streamRecordsStatus)
      setstreamRecordStatusDataSet(
        setStreamRecordStatus(props.streamRecordsStatus)
      );
    setProductViolators(props.productViolators);
    setViolationCauses(props.violationsCauseReport);
  }, [
    props.mainCategoryViolationStatus,
    props.streamRecordsStatus,
    props.productViolators,
    props.violationsCauseReport
  ]);
  //console.log("product violators " + JSON.stringify(productViolators));
 

  if (productViolators && productViolators.length >= 1) {
    productViolatorsToList = productViolators.map(item => {
      return (
        <tr>
          <td className="edit" key={item.prooductId}>
            {item.productDetails.name}
          </td>
          <td className="edit" key={item.prooductId}>
            {subString(item.productDetails.companyDetails.name)}
          </td>
          <td className="edit" key={item.prooductId}>
            {item.violationcount}
          </td>
        </tr>
      );
    });
  }


  if (violationCauses && violationCauses.length >= 1) {
    
    violationCausesToList = violationCauses.map(item => {
      return (
        <tr>
          <td className="edit">
            {/* {item.violationCause} */}
             {
              violations.filter((violation) => violation.id == item.violationCause)[0].name
            }
          </td>
          <td className="edit">
            {item.count}
          </td>
         
        </tr>
      );
    });
  }
  return (
    
    <div>
      <MDBRow className="mb-4">
        <MDBCol md="6" className="mb-4">
          <MDBCard>
            <MDBCardBody>
              <MDBRow>
              <MDBCol size="8">
                <text style={{fontSize: "1.5rem", }}><strong>Monitor Streams Status</strong></text>
              </MDBCol>
              <MDBCol size="4" className="d-flex justify-content-end">
                <h3>
                  <strong>
                  <u>{props.streamRecordsStatus.total}</u>
                  </strong>
                </h3>
              </MDBCol>
              
              </MDBRow>
              <p></p>
              <MDBRow>
                <MDBCol>
                <p> Compliant:  <b>{props.streamRecordsStatus.compliance}</b></p>
                </MDBCol>
                <MDBCol>
                <p> Non-Compliant: <b>{props.streamRecordsStatus.nonComplience}</b></p>
                </MDBCol>
                <MDBCol>
                <p> Potential Non-Compliant:  <b>{props.streamRecordsStatus.potentialNonComplience}</b></p>
                </MDBCol>
                </MDBRow>
              <hr />
              <MDBCardBody style={{ height: "340px", maxHeight: "450px" }}>
                <Doughnut
                  data={streamRecordStatusDataSet}
                  height={50}
                  options={{ maintainAspectRatio: false }}
                  
                />
                <br />
                <br />
                <br />
                <br />

              </MDBCardBody>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>

        <MDBCol md="6" className="mb-4">
          <MDBCard>
            <MDBCardBody style={{height: "450px", maxHeight: "450px" }}>
            <text style={{fontSize: "1.5rem", }}><strong>Top Violators by Product</strong></text>
              <hr />
              <MDBTable hover>
                <MDBTableHead color="orange">
                  <tr>
                    <th>Brand</th>
                    <th>Company</th>
                    <th>Total</th>
                  </tr>
                </MDBTableHead>
                <MDBTableBody>{productViolatorsToList}</MDBTableBody>
              </MDBTable>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>

      <MDBRow className="mb-4">
        <MDBCol md="6" className="mb-4">
          <MDBCard className="mb-4">
            <MDBCardBody style={{ height: "450px", maxHeight: "450px" }}>
            <MDBRow>
              <MDBCol size="8">
                <text style={{fontSize: "1.5rem", }}><strong>Top Violations by Main Category</strong></text>
              </MDBCol>
              <MDBCol size="4" className="d-flex justify-content-end">
                <h3>
                  <strong>
                  <u>{props.mainCategoryViolationStatus.total}</u>
                  </strong>
                </h3>
              </MDBCol>
              </MDBRow>
            
              <hr />
              <MDBCardBody>
                <HorizontalBar
                  data={mainCategoryDataSet}
                  height={135}
                  options={{
                    scales: {
                      xAxes: [
                        {
                          stacked: true
                        }
                      ],
                      yAxes: [
                        {
                          stacked: true
                        }
                      ]
                    }
                  }}
                />
              </MDBCardBody>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
        <MDBCol md="6" className="mb-4">
          <MDBCard>
            <MDBCardBody style={{height: "450px", maxHeight: "450px" }}>
            <text style={{fontSize: "1.5rem", }}><strong>Top Violations</strong></text>
              <hr />
              <MDBTable hover>
                <MDBTableHead color="orange">
                  <tr>
                    <th>Violations</th>
                    <th>Total</th>
                  </tr>
                </MDBTableHead>
                <MDBTableBody>
                  {violationCausesToList}
                </MDBTableBody>
              </MDBTable>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </div>
  );
};

const mapStateToProps = state => ({
  mainCategoryViolationStatus: state.data.mainCategoryViolationStatus,
  streamRecordsStatus: state.data.streamRecordsStatus,
  productViolators: state.data.productViolators,
  violationsCauseReport: state.data.violationsCauseReport
});

export default connect(mapStateToProps)(DashboardCharts);
