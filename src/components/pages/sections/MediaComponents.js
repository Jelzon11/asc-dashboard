import React, { useEffect, useState } from "react";
import { MDBCard, MDBCardBody, MDBRow, MDBCol, MDBCardHeader } from "mdbreact";
import { connect } from "react-redux";
import { subString } from "../../utils";
import { Link } from "react-router-dom";

const MediaComponents = props => {
  const [violators, setViolators] = useState([]);
  const [company, setCompany] = useState([]);
  let violatorsToList = [];

  useEffect(() => {
    setViolators(props.companyViolators.companylist);
    setCompany(props.companyViolators.companylist);
  }, [props.companyViolators]);
  //console.log(violators)

  if (violators && violators.length >= 1) {
    violatorsToList = violators.map(item => {
      return (
        
          <li className="edit" key={item.companyId}>
            <Link className="hover"to={{
              pathname: `/companyreports/${item.companyId}`
            }}
            >
              {subString(item.companyDetails.name)}
            </Link>
            <span
              style={{ float: "right", color: "white", fontWeight: "bold" }}
              className="dot"
            >
              !
            </span>
          </li>
      
      );
    });
  }
  return (
    <MDBRow>
      <MDBCol style={{ padding: 10 }}>
        <MDBCard className="cascading-admin-card">
          <MDBCardHeader>
            <MDBRow>
              <MDBCol size="7">
                <strong>Radio</strong>
              </MDBCol>
              <MDBCol size="5" className="d-flex justify-content-end">
                <h3>
                  <strong>
                    {props &&
                      props.companyViolators &&
                      props.companyViolators.total}
                  </strong>
                </h3>
              </MDBCol>
            </MDBRow>
          </MDBCardHeader>
          <MDBCardBody>
            <MDBRow
              className="customScrollbar"
              style={{height: "150px", maxHeight: "150px", overflowY: "scroll" }}
            >
              <ol class="orderedList">
                {violatorsToList}
              </ol>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>

      <MDBCol style={{ padding: 10 }}>
        <MDBCard className="cascading-admin-card">
          <MDBCardHeader>
            <MDBRow>
              <MDBCol size="7">
                <strong>Online</strong>
              </MDBCol>
              <MDBCol size="5" className="d-flex justify-content-end">
                <h3>
                  <strong>#</strong>
                </h3>
              </MDBCol>
            </MDBRow>
          </MDBCardHeader>
          <MDBCardBody>
            <MDBRow
              className="customScrollbar"
              style={{ maxHeight: "150px", overflowY: "scroll" }}
            >
              <h1>N/A</h1>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>

      <MDBCol style={{ padding: 10 }}>
        <MDBCard className="cascading-admin-card">
          <MDBCardHeader>
            <MDBRow>
              <MDBCol size="7">
                <strong>TV</strong>
              </MDBCol>
              <MDBCol size="5" className="d-flex justify-content-end">
                <h3>
                  <strong>#</strong>
                </h3>
              </MDBCol>
            </MDBRow>
          </MDBCardHeader>
          <MDBCardBody>
            <MDBRow
              className="customScrollbar"
              style={{ maxHeight: "150px", overflowY: "scroll" }}
            >
              <h1>N/A</h1>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>

      <MDBCol style={{ padding: 10 }}>
        <MDBCard className="cascading-admin-card">
          <MDBCardHeader>
            <MDBRow>
              <MDBCol size="7">
                <strong>OOH</strong>
              </MDBCol>
              <MDBCol size="5" className="d-flex justify-content-end">
                <h3>
                  <strong>#</strong>
                </h3>
              </MDBCol>
            </MDBRow>
          </MDBCardHeader>
          <MDBCardBody>
            <MDBRow
              className="customScrollbar"
              style={{ maxHeight: "150px", overflowY: "scroll" }}
            >
              <h1>N/A</h1>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>

      <MDBCol style={{ padding: 10 }}>
        <MDBCard className="cascading-admin-card">
          <MDBCardHeader>
            <MDBRow>
              <MDBCol size="7">
                <strong>Print Ads</strong>
              </MDBCol>
              <MDBCol size="5" className="d-flex justify-content-end">
                <h3>
                  <strong>#</strong>
                </h3>
              </MDBCol>
            </MDBRow>
          </MDBCardHeader>
          <MDBCardBody>
            <MDBRow
              className="customScrollbar"
              style={{ maxHeight: "150px", overflowY: "scroll" }}
            >
              <h1>N/A</h1>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  );
};
const mapStateToProps = state => ({
  companyViolators: state.data.companyViolators
});

export default connect(mapStateToProps)(MediaComponents);
