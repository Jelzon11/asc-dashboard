import React, { useState } from "react";
import { LogoutUser } from "../../actions/companyAction";
import { removeToken } from '../utils';
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBCollapse,
  MDBNavItem,
  MDBNavLink,
  MDBIcon,
  MDBDropdown,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBDropdownToggle
} from "mdbreact";

const TopNavigation = props => {
  const logoutOnclickHandler = () => {
    LogoutUser(() => {
      removeToken();
      window.location = '/'
    });
  };

  return (
    <MDBNavbar
      style={{ backgroundColor: "#ffff" }}
      className="main-header navbar navbar-expand navbar-light"
    >
      <MDBNavbarNav left>
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" data-widget="pushmenu" href="#">
              {/* <i style={{ color: "black" }} className="fas fa-bars"></i> */}
              <MDBIcon icon="bars" className="mr-3" size="lg" />
            </a>
          </li>
        </ul>
      </MDBNavbarNav>
      <MDBNavbarNav right>
        <a onClick={() => logoutOnclickHandler()} className="nav-link" href="#">
          <i
            style={{ color: "#fffff" }}
            className="fas fa-power-off fa-lg"
          ></i>
        </a>
      </MDBNavbarNav>
    </MDBNavbar>
  );
};

export default TopNavigation
