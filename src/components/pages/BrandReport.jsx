import React, { useState, useEffect, Fragment } from "react";
import { Link } from 'react-router-dom';
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
 MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBCol,
  MDBRow,
  MDBDataTable,
  MDBIcon,
  MDBBtn
} from "mdbreact";
import { Doughnut, Pie } from "react-chartjs-2";
import data from "../Utils/listOfProducts";

const columns = () => {
  return [
    {
      label: "Product name",
      field: "name",
      sort: "asc",
      width: 150
    },
    {
      label: "No. of Materials",
      field: "noOfProducts",
      sort: "asc",
      width: 270
    },
    {
      label: "Main Category",
      field: "mainCategory",
      sort: "asc",
      width: 270
    },
    {
      label: "Sub Category",
      field: "subCategory",
      sort: "asc",
      width: 270
    },
    {
      label: "Sub Sub-Category",
      field: "subSubCategory",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Compliance",
      field: "totalCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Non-Compliance",
      field: "totalNonCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 270
    }
  ];
};

const BrandReport = props => {
 
 
  const [productList, setProductList] = useState([]);

  

  const dataDoughnut = {
    labels: ["Red", "Green", "Yellow", "Grey"],
    datasets: [
      {
        data: [300, 50, 100, 40],
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
      }
    ]
  };

  const dataPie = {
    labels: ["Brand 1", "Brand 2", "Brand 3", "Brand 4", "Brand 5"],
    datasets: [
      {
        data: [300, 50, 100, 40, 120],
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
      }
    ]
  };

  const renderData = productList => {
    //console.log(productList);
    const newProductList = productList.map(product => {
      return {
        name: product.name,
        noOfProducts: product.noOfProducts,
        mainCategory: product.mainCategory,
        subCategory: product.subCategory,
        subSubCategory: product.subSubCategory,
        totalCompliance: product.totalCompliance,
        totalNonCompliance: product.totalNonCompliance,
        action: (
          <Fragment>
            <center>
            <Link to={{pathname: '/productReport', state: { product: product, prevPath: props.location }}}><MDBBtn  color="light-blue" size="sm">
                <MDBIcon icon="eye" size="2x" color="white"/>
            </MDBBtn></Link>
            </center>
          </Fragment>
        )
      };
    });
    return newProductList;
  };

  const [location1, setLocation1] = useState("");
  const [location2, setLocation2] = useState("");
  const [location3, setLocation3] = useState("");

  useEffect(() => {
    
    setProductList(data);
    setLocation1(props.location &&
      props.location.state &&
      props.location.state.prevPath &&
      props.location.state.prevPath.state.prevPath.pathname);
    setLocation2(props.location &&
      props.location.state &&
      props.location.state.prevPath &&
      props.location.state.prevPath.state.company.name);
    setLocation3(props.location &&
      props.location.state &&
      props.location.state.brand &&
      props.location.state.brand.name);
    
  }, []);
  
 // console.log(props);
  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBCard className="mb-5">
              <MDBCardHeader>
                <h1>
                  {location3}{" "}
                  Report
                </h1>
                 <MDBBreadcrumb>
                 
                  <MDBBreadcrumbItem>
                  <Link to='#'
                    >
                      {location1}{" "}
                      </Link>
                    <Link to='#'
                    >
                      /{location2}{" "}
                      </Link>
                  </MDBBreadcrumbItem>
                  <MDBBreadcrumbItem active>
                    {location3}{" "}
                    {" "}
                  </MDBBreadcrumbItem>
                </MDBBreadcrumb> 
              </MDBCardHeader>
              <MDBCardBody>
                  <MDBRow className="mb-12">
                  <MDBCol md="2" lg="2" className="mb-2">
                    </MDBCol>
                    <MDBCol md="4" lg="4" className="mb-4">
                      
                          <Doughnut
                            data={dataDoughnut}
                            height={200}
                            options={{ responsive: true }}
                          />
                    </MDBCol>
                    <MDBCol md="4" className="mb-4">
                     
                          <Pie
                            data={dataPie}
                            height={200}
                            options={{ responsive: true }}
                          />
                    </MDBCol>
                    <MDBCol md="2" lg="2" className="mb-2">
                    </MDBCol>
                  </MDBRow>

                  <MDBCard className="mb-5">
              <MDBCardHeader
                tag="h4"
                className="font-weight-bold text-uppercase"
              >
                Product List
              </MDBCardHeader>
              <MDBCardBody className="d-flex align-items-center justify-content-between">
                <MDBDataTable
                  striped
                  fixed={true}
                  small
                  bordered
                  hover
                  order={["name", "desc"]}
                  disableRetreatAfterSorting={true}
                  sortable={true}
                  responsive={true}
                  searchLabel={"Search..."}
                  responsiveSm={true}
                  responsiveLg={true}
                  responsiveXl={true}
                  noBottomColumns={true}
                  data={{
                    rows: renderData(productList),
                    columns: columns()
                  }}
                />
              </MDBCardBody>
            </MDBCard>
                 
                {/* </MDBContainer> */}
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  );
};

export default BrandReport;
