import React, { Fragment, useState, useEffect } from "react";
import{Link} from 'react-router-dom';
import {
  MDBCard,
  MDBCardBody,
  MDBDataTable,
  MDBCardHeader,
  MDBBtn,
  MDBIcon,
  MDBCardText,
  MDBBreadcrumb,
  MDBBreadcrumbItem
} from "mdbreact";
import { connect } from "react-redux";
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
const _companyReportPage = 'e3c87610-d227-47cc-a211-1a2792bbcd55';

const columns = () => {
  return [
    {
      label: "Company name",
      field: "name",
      sort: "asc",
      width: 300
    },
    {
      label: "No. of Product",
      field: "noOfProduct",
      sort: "asc",
      width: '10%'
    },
    {
      label: "Compliant",
      field: "totalComplied",
      sort: "asc",
      width: '10%'
    },
    {
      label: "Non-Compliant",
      field: "totalNonComplied",
      sort: "asc",
      width: '10%',
    },
    {
      label: "Potential Non-Compliance",
      field: "totalPotentialNonCompliance",
      sort: "asc",
      width: '10%'
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: '20%'
    }
  ];
};

const Reports = (props) => {
  const [companyList, setCompanyList] = useState([]);

  useEffect(() => {
    setCompanyList(props.companyReports);
  }, [props.companyReports]);

  const renderData = companyList => {
    const newCompanyList = companyList.map(company => {
      return {
        name: company.name,
        noOfProduct: company.productcount,
        totalComplied: (<MDBCardText style={{color: 'green', fontWeight: 'bold'}}>{company.compliedCount}</MDBCardText>),
        totalNonComplied: (<MDBCardText style={{color: 'red', fontWeight: 'bold'}}>{company.confirmedCount}</MDBCardText>) ,
        totalPotentialNonCompliance: (<MDBCardText style={{color: 'orange', fontWeight: 'bold'}}>{company.nonCompliedCount}</MDBCardText>),
        action: (
          <Fragment>
            <center>
              <Link to={{pathname: '/companyreports/' + company.id}}>
                <MDBBtn  color="light-blue" size="sm">
                <MDBIcon icon="eye" size="1x" className="eye-button"/>
                </MDBBtn>
              </Link>
            </center>
          </Fragment>
        )
      };
    });
    return newCompanyList;
  };
  return isAuthorize(_companyReportPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
          <br />
            <MDBBreadcrumb className={'breadCrumb'}>
              <MDBBreadcrumbItem>
                <Link 
                  className={'breadCrumb-link'}
                  to={{ pathname: "/companyreports" }} active
                >
                  COMPANIES
                </Link>
              </MDBBreadcrumbItem>
            </MDBBreadcrumb>
            <br />
            <MDBCard className="mb-5">
              <MDBCardHeader
                tag="h4"
                className="text-uppercase"
              >
                Companies
              </MDBCardHeader>
              <MDBCardBody className="d-flex align-items-center justify-content-between">
                <MDBDataTable
                  striped
                  fixed={true}
                  small
                  bordered
                  hover
                  order={["name", "desc"]}
                  disableRetreatAfterSorting={true}
                  sortable={true}
                  responsive={true}
                  searchLabel={"Search..."}
                  responsiveSm={true}
                  responsiveLg={true}
                  responsiveXl={true}
                  noBottomColumns={true}
                  data={{
                    rows: renderData(companyList),
                    columns: columns()
                  }}
                />
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};

const mapStateToProps = state => ({
  companyReports: state.data.companyReports
});

export default connect(mapStateToProps)(Reports);
