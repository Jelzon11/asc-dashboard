import React, { Fragment, useState, useEffect } from "react";
import moment from 'moment';
import {
  MDBCard,
  MDBCardBody,
  MDBDataTable,
  MDBCardHeader,
  MDBBtn,
  MDBCol,
  MDBRow
} from "mdbreact";
import { connect } from "react-redux";
import ViewModalNonExisting from "./modals/viewModalNonExisting";
import DateRangePickerNonExistingMaterials from './DateRangePickerNonExistingMaterials';
import { fetchNonExistingMaterials } from "../../actions/companyAction";
import { generateReport } from '../utils';
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
const _nonExistingMaterialPage = '14d79e6f-6597-457e-aa12-f77807c6e1e3'

const columns = () => {
  return [{
    label: "Company name",
    field: "company",
    sort: "asc",
    width: 300
  },
  {
    label: "Brand Name",
    field: "brand",
    sort: "asc",
    width: '10%'
  },
  {
    label: "Product",
    field: "product",
    sort: "asc",
    width: '10%'
  },
  {
    label: "Main-Category",
    field: "mainCategory",
    sort: "asc",
    width: '10%',
  },
  {
    label: "Sub-Category",
    field: "subCategory",
    sort: "asc",
    width: '10%'
  },
  {
    label: "Duration",
    field: "duration",
    sort: "asc",
    width: '10%'
  },
  {
    label: "Timestamp",
    field: "timestamp",
    sort: "asc",
    width: '10%'
  },
  {
    label: "Material",
    field: "material",
    sort: "asc",
    width: '10%'
  }
  ]
}

const reportColumn = [{
  key: 'id',
  header: 'Record ID'
}, {
  key: 'duration',
  header: 'Duration'
}, {
  key: 'channel',
  header: 'Channel'
}, {
  key: 'city',
  header: 'City'
}, {
  key: 'region',
  header: 'Region'
}, {
  key: 'reason',
  header: 'Reason'
}, {
  key: 'companyName',
  header: 'Company'
}, {
  key: 'productName',
  header: 'Product'
}, {
  key: 'mainCategory',
  header: 'Category'
}, {
  key: 'startRecordTimestamp',
  header: 'DateTime'
}];

const NonExistingMaterialReports = (props) => {
  const [nonExistingMaterialsList, setNonExistingMaterialsList] = useState([]);
  const [nonExistingMaterials, setNonExistingMaterials] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [nonExistingRecord, setNonExistingRecord] = useState({});

  if (Object.keys(nonExistingMaterials).length <= 0) {
    props.fetchNonExistingMaterials();
  }

  if (props.nonExistingMaterials && props.nonExistingMaterials.notInTheArchiveList) {
    setNonExistingMaterialsList(props.nonExistingMaterials.notInTheArchiveList);
    delete props.nonExistingMaterials['notInTheArchiveList'];
    setNonExistingMaterials(props.nonExistingMaterials);
  }

  const generateXMLReport = () => {
    generateReport({
      column: reportColumn,
      data: nonExistingMaterialsList,
      subject: 'Non-ASC Material',
      workBookTitle: 'sheet'
    });
  }

  useEffect(() => {

  }, [props.nonExistingMaterials]);

  const handleViewModal = (streamId) => {
    if (isModalOpen == false) {
      const streamRecord = nonExistingMaterialsList.find(item => item.id == streamId);
      setNonExistingRecord(streamRecord);
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };
  const renderData = nonExistingMaterialsList => {
    const newNonExistingMaterialsList = nonExistingMaterialsList && nonExistingMaterialsList.map(nonExistingMaterial => {
      return {
        company: nonExistingMaterial.companyName,
        brand: nonExistingMaterial.brandName,
        product: nonExistingMaterial.productName,
        mainCategory: nonExistingMaterial.mainCategory,
        subCategory: nonExistingMaterial.subCategory,
        duration: nonExistingMaterial.duration,
        timestamp: moment.unix(nonExistingMaterial.startRecordTimestamp).format('MM/DD/YYYY hh:mm:ss a'),
        material: (
          <Fragment>
            <audio
              src={nonExistingMaterial.fileURL}
              controls
              style={{ width: "100%" }}
            ></audio>
          </Fragment>
        ),
        action: (<Fragment>
          <center>
            <MDBBtn
              onClick={() => handleViewModal(nonExistingMaterial.id)}
              color="light-blue"
              size="sm"
            >
              <font style={{ color: "white" }}>View</font>
            </MDBBtn>
          </center>
        </Fragment>)
      };
    });
    return newNonExistingMaterialsList;
  };
  return isAuthorize(_nonExistingMaterialPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBCard className="mb-5">
              <ViewModalNonExisting
                isModalOpen={isModalOpen}
                handleViewModal={handleViewModal}
                nonExistingRecord={nonExistingRecord}
              // materialFullDetails={materialFullDetails}
              />
              <MDBCardHeader
                tag="h4"
                className="text-uppercase"
              >
                <MDBRow>
                  <MDBCol size="7">
                    Non-ASC Materials
                <p style={{ marginTop: "10px", fontSize: "1rem", }}>
                      Total:
                  <text
                        className="mt-2"
                        style={{
                          padding: "5px",
                          fontWeight: "500",
                          color: "#4692d4"
                        }}
                      >
                        {props.nonExistingMaterials && props.nonExistingMaterials.total}
                      </text>
                    </p>
                  </MDBCol>
                  <MDBCol size="5">
                    <DateRangePickerNonExistingMaterials />
                  </MDBCol>
                </MDBRow>
                <MDBCol size="5"></MDBCol>
                <MDBCol>
                  <MDBBtn onClick={() => generateXMLReport()}>XLSX Report</MDBBtn>
                </MDBCol>
              </MDBCardHeader>

              <MDBCardBody className="d-flex align-items-center justify-content-between">
                <MDBDataTable
                  striped
                  fixed={true}
                  small
                  bordered
                  hover
                  order={["name", "desc"]}
                  disableRetreatAfterSorting={true}
                  sortable={true}
                  responsive={true}
                  searchLabel={"Search..."}
                  responsiveSm={true}
                  responsiveLg={true}
                  responsiveXl={true}
                  noBottomColumns={true}
                  data={{
                    rows: (nonExistingMaterialsList && nonExistingMaterialsList.length >= 1) ? renderData(nonExistingMaterialsList) : [],
                    columns: columns()
                  }}
                />
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};

const mapStateToProps = state => ({
  nonExistingMaterials: state.data.nonExistingMaterials
});


export default connect(mapStateToProps, { fetchNonExistingMaterials })(NonExistingMaterialReports)
