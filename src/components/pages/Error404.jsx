import React, { } from 'react';
import { MDBCard, MDBCardBody, MDBCardHeader } from 'mdbreact';

const Error404 = () => {
  return (
      <div className="content-wrapper">
        <div className="content">
          <div className="container-fluid">
            <div className="col-md-12">
              <br />
              <MDBCard className="mb-5">
                <MDBCardHeader>
                    <h1>Error 404</h1>
                </MDBCardHeader>
              <MDBCardBody
                className="d-flex align-items-center justify-content-between"
              >
               <p>Page is not found</p>
              </MDBCardBody>
            </MDBCard>
            </div>
            <br />
          </div>
        </div>
      </div>
  )
}

export default Error404;