import React, { useState, useEffect, Fragment } from "react";
import moment from "moment-timezone";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCol,
  MDBRow,
  MDBDataTable,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBBtn
} from "mdbreact";

import ViewModalRadioMaterial from "./modals/viewModalRadioMaterial";
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
import { fetchMaterialStreamRecordsReportData } from "../../actions/companyAction";
const _companyProductMaterialStreamRecordReportPage = '338d8533-cb2e-4f5d-bb1b-15f81f59aa47';

const columns = () => {
  return [
    {
      label: "Station",
      field: "channel",
      sort: "asc",
      width: 150
    },
    {
      label: "Status",
      field: "status",
      sort: "asc",
      width: 270
    },
    {
      label: "Region",
      field: "region",
      sort: "asc",
      width: 270
    },
    {
      label: "City",
      field: "city",
      sort: "asc",
      width: 270
    },
    {
      label: "Date Aired",
      field: "timestamp",
      sort: "asc",
      width: 270
    },
    {
      label: "Aired Material",
      field: "airedMaterial",
      sort: "asc",
      width: 270
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 270
    }
  ];
};

const MaterialReport = props => {
  const [radioStreamedMaterialList, setRadioStreamedMaterialList] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const [materialFullDetails, setMaterialFullDetail] = useState({});
  const [selectecSteamRecord, setSelectecSteamRecord] = useState({});

  const handleViewModal = (streamId) => {
    if (isModalOpen == false) {
      const streamRecord = radioStreamedMaterialList.find(item => item.id == streamId);
      setSelectecSteamRecord(streamRecord);
      setModalOpen(true);
    } else {
      setModalOpen(false);
    }
  };

  const { productId, companyId, materialId } = props.match.params;

  if (Object.keys(materialFullDetails).length <= 0) props.fetchMaterialStreamRecordsReportData(materialId);

  if (
    props.materialStreamRecordsReport &&
    props.materialStreamRecordsReport.streamRecords
  ) {
    setRadioStreamedMaterialList(props.materialStreamRecordsReport.streamRecords);
    delete props.materialStreamRecordsReport["streamRecords"];
    setMaterialFullDetail(props.materialStreamRecordsReport);
  }

  useEffect(() => {}, [props.materialStreamRecordsReport]);

  const renderData = radioStreamedMaterialList => {
    //console.log(companyList);
    const newRadioMaterialList = radioStreamedMaterialList.map(
      radioMaterial => {
        return {
          channel: radioMaterial.channel,
          status: radioMaterial.status,
          region: radioMaterial.region,
          city: radioMaterial.city,
          timestamp: moment.unix(radioMaterial.startRecordTimestamp).tz('Asia/Manila').format("MM/DD/YYYY hh:mm:ss a"),
          //radioMaterial.startRecordTimestamp,
          airedMaterial: (
            <Fragment>
              <audio
                src={radioMaterial.fileURL}
                controls
                style={{ width: "100%" }}
              ></audio>
            </Fragment>
          ),
          action: (
            <Fragment>
              <center>
                <MDBBtn
                  onClick={() => handleViewModal(radioMaterial.id)}
                  color="light-blue"
                  size="sm"
                  
                >
                  <font style={{color: "white"}}>View </font>
                </MDBBtn>
                {radioMaterial.status == "Compliant" ? (
                  <span>
                    <i class="far fa-check-circle green-text"></i>
                  </span>
                ) : radioMaterial.status == "Non-Compliant" ? (
                  <span>
                    <i class="fas fa-exclamation-circle red-text"></i>
                  </span>
                ) : (
                  <span>
                    <i class="fas fa-exclamation-circle orange-text"></i>
                  </span>
                )}
              </center>
            </Fragment>
          )
        };
      }
    );
    return newRadioMaterialList;
  };
  //console.log(materialFullDetails)
  return isAuthorize(_companyProductMaterialStreamRecordReportPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
          <br />
            <MDBBreadcrumb className={'breadCrumb'}>
              <MDBBreadcrumbItem>
                <Link 
                  className={'breadCrumb-link'}
                  to={{ pathname: "/companyreports" }}
                >
                  COMPANIES
                </Link>
              </MDBBreadcrumbItem>
              <MDBBreadcrumbItem>
                <Link
                  className={'breadCrumb-link'}
                  to={{
                    pathname: `/companyreports/${companyId}`
                  }}
                >
                  {materialFullDetails && materialFullDetails.productDetails 
                  && materialFullDetails.productDetails.companyDetails 
                  && materialFullDetails.productDetails.companyDetails.name}
                </Link>
              </MDBBreadcrumbItem>
              <MDBBreadcrumbItem>
                <Link
                   className={'breadCrumb-link'}
                  to={{
                    pathname: `/companyreports/${companyId}/productreports/${productId}`
                  }}
                >
                  {materialFullDetails && materialFullDetails.productDetails && materialFullDetails.productDetails.name }
                </Link>
              </MDBBreadcrumbItem>
              <MDBBreadcrumbItem active>
                <Link
                  className={'breadCrumb-link'}
                  to={{
                    pathname: `/companyreports/${companyId}/productreports/${productId}/materialreports/${materialId}`
                  }}
                >
                  {materialFullDetails && materialFullDetails.title}
                </Link>
              </MDBBreadcrumbItem> 
            </MDBBreadcrumb>
            <br />
            <MDBCard className="">
              <ViewModalRadioMaterial
                isModalOpen={isModalOpen}
                handleViewModal={handleViewModal}
                selectedRecordStream={selectecSteamRecord}
                materialFullDetails={materialFullDetails}
              />
              <MDBCardHeader>
                <h3>{materialFullDetails && materialFullDetails.title}</h3>
                <h5>
                  RC:
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "20px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "#4692d4"
                    }}
                  >
                    {materialFullDetails && materialFullDetails.referenceId}
                  </text>
                </h5>
                {/* <MDBBreadcrumb>
                  <MDBBreadcrumbItem>
                    <Link to="#">{location1} </Link>
                  </MDBBreadcrumbItem>
                  <MDBBreadcrumbItem active>
                    <Link to="#">{location2} </Link>
                  </MDBBreadcrumbItem>
                  <MDBBreadcrumbItem>
                    <Link to="#">{location3} </Link>{" "}
                  </MDBBreadcrumbItem>
                   <MDBBreadcrumbItem active>{location4} </MDBBreadcrumbItem> 
                </MDBBreadcrumb> */}
              </MDBCardHeader>
              <MDBCardBody>
                <h5>
                  Total Non-Compliant:{" "}
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "30px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "red"
                    }}
                  >
                    {materialFullDetails && materialFullDetails.nonCompliant}
                  </text>
                </h5>
                <h5>
                  Total Potential Non-Compliant:{" "}
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "30px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "orange"
                    }}
                  >
                    {materialFullDetails &&
                      materialFullDetails.potentialNonCompliant}
                  </text>
                </h5>
                <br />
                <MDBCard className="mb-5">
                  <MDBCardHeader
                    tag="h4"
                    className="text-uppercase"
                  >
                    <MDBRow>
                      <MDBCol md="8">Station List</MDBCol>
                      <MDBCol>
                        <text style={{ color: "#f5633b" }}>
                          Original Material
                        </text>
                        <audio
                          src={
                            materialFullDetails && materialFullDetails.fileURL + "#t=5s"
                          }
                          oncanplaythrough="this.play();"
                          controls
                          style={{ width: "100%" }}
                        ></audio>
                      </MDBCol>
                    </MDBRow>
                  </MDBCardHeader>
                  <MDBCardBody className="d-flex align-items-center justify-content-between">
                    <MDBDataTable
                      striped
                      fixed={true}
                      small
                      bordered
                      hover
                      order={["name", "desc"]}
                      disableRetreatAfterSorting={true}
                      sortable={true}
                      responsive={true}
                      searchLabel={"Search..."}
                      responsiveSm={true}
                      responsiveLg={true}
                      responsiveXl={true}
                      noBottomColumns={true}
                      data={{
                        rows: renderData(radioStreamedMaterialList),
                        columns: columns()
                      }}
                    />
                  </MDBCardBody>
                </MDBCard>

                {/* </MDBContainer> */}
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};
MaterialReport.propTypes = {
  fetchMaterialStreamRecordsReportData: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  materialStreamRecordsReport: state.data.materialStreamRecordsReport
});

export default connect(mapStateToProps, {
  fetchMaterialStreamRecordsReportData
})(MaterialReport);
