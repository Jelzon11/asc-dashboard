import React, { Fragment} from 'react';

const Select = (props) => {
  return(
    <Fragment>
      <select 
        name={props.name}
        onChange={e => props.onChange(e)} 
        className="browser-default custom-slect form-control"
        disabled={props.disabled}
        defaultValue={props.value && props.value}
      >
        {(!props.noChooseOption == true) && <option value={0}>Choose your option</option>}
        {props.items && props.items.map((item, index) => {
          return (
            <Option
              key={index}
              name={item.name}
              value={item.id}
            />
          )
        })}
      </select>
    </Fragment>
  );
}

const Option = (props) => {
  return (
    <option key={props.key} id={props.name} value={props.value}>
      {props.name}
    </option>
  )
}

export default Select;