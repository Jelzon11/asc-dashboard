import React, { useState, useEffect, Fragment } from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBCol,
  MDBRow,
  MDBDataTable,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBBtn
} from "mdbreact";
import { Doughnut, Pie } from "react-chartjs-2";
import data from "../utils/listOfPlatforms";

const columns = () => {
  return [
    {
      label: "Online Platform",
      field: "name",
      sort: "asc",
      width: 150
    },
    {
      label: "Status",
      field: "status",
      sort: "asc",
      width: 270
    },
    {
      label: "Region",
      field: "region",
      sort: "asc",
      width: 270
    },
    {
      label: "Province",
      field: "province",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Compliance",
      field: "totalCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Total Non-Compliance",
      field: "totalNonCompliance",
      sort: "asc",
      width: 270
    },
    {
      label: "Aired Material",
      field: "airedMaterial",
      sort: "asc",
      width: 270
    },
    {
      label: "Action",
      field: "action",
      sort: "asc",
      width: 270
    }
  ];
};

const MaterialReport = props => {
  const [onlineMaterialList, setOnlineMaterialList] = useState([]);

  useEffect(() => {
    //setProductDetails(props.location.state);
    setOnlineMaterialList(data);
  }, []);

  const dataDoughnut = {
    labels: ["Red", "Green", "Yellow", "Grey"],
    datasets: [
      {
        data: [300, 50, 100, 40],
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
      }
    ]
  };

  const dataPie = {
    labels: ["Brand 1", "Brand 2", "Brand 3", "Brand 4", "Brand 5"],
    datasets: [
      {
        data: [300, 50, 100, 40, 120],
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1"],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5"]
      }
    ]
  };


  const renderData = onlineMaterialList => {
  
    const newRadioMaterialList = onlineMaterialList.map(radioMaterial => {
      return {
        name: radioMaterial.name,
        status: radioMaterial.status,
        region: radioMaterial.region,
        province: radioMaterial.province,
        totalCompliance: radioMaterial.totalCompliance,
        totalNonCompliance: radioMaterial.totalNonCompliance,
        

        airedMaterial: (
          <Fragment>
            <audio
              src={radioMaterial.airedMaterial}
              controls
              style={{ width: "100%" }}
            ></audio>
          </Fragment>
        ),
        action: (
          <Fragment>
            <center>
              <MDBBtn
                // onClick={() => {}}
                color="light-blue"
                size="sm"
              >
               View
              </MDBBtn>
            </center>
          </Fragment>
        )
      };
    });
    return newRadioMaterialList;
  };


  return (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBCard className="">
              <MDBCardHeader>
                <h1>
                  {props.location &&
                    props.location.state &&
                    props.location.state.onlineMaterial &&
                    props.location.state.onlineMaterial.name}{" "}
                  Report
                </h1>

                <MDBBreadcrumb>
                  <MDBBreadcrumbItem active>Home</MDBBreadcrumbItem>
                </MDBBreadcrumb>
              </MDBCardHeader>
              <MDBCardBody>
                <h3
                  className="mt-2"
                  style={{
                    padding: "10px",
                    fontWeight: "500",
                    color: "#f5633b"
                  }}
                >
                  Total Violation: 219
                </h3>

                <br />
                <MDBRow className="mb-12">
                  <MDBCol md="2" lg="2" className="mb-2"></MDBCol>
                  <MDBCol md="4" lg="4" className="mb-4">
                    <Doughnut
                      data={dataDoughnut}
                      height={200}
                      options={{ responsive: true }}
                    />
                  </MDBCol>
                  <MDBCol md="4" className="mb-4">
                    <Pie
                      data={dataPie}
                      height={200}
                      options={{ responsive: true }}
                    />
                  </MDBCol>
                </MDBRow>

                <MDBCard className="mb-5">
                  <MDBCardHeader
                    tag="h4"
                    className="font-weight-bold text-uppercase"
                  >
                    <MDBRow>
                      <MDBCol md="8">Station List</MDBCol>

                      <MDBCol>
                        <text style={{ color: "#f5633b" }}>
                          Original Material
                        </text>
                        <audio
                          src=""
                          controls
                          style={{ width: "100%" }}
                        ></audio>
                      </MDBCol>
                    </MDBRow>
                  </MDBCardHeader>
                  <MDBCardBody className="d-flex align-items-center justify-content-between">
                    <MDBDataTable
                      striped
                      fixed={true}
                      small
                      bordered
                      hover
                      order={["name", "desc"]}
                      disableRetreatAfterSorting={true}
                      sortable={true}
                      responsive={true}
                      searchLabel={"Search..."}
                      responsiveSm={true}
                      responsiveLg={true}
                      responsiveXl={true}
                      noBottomColumns={true}
                      data={{
                        rows: renderData(onlineMaterialList),
                        columns: columns()
                      }}
                    />
                  </MDBCardBody>
                </MDBCard>

                {/* </MDBContainer> */}
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  );
};

export default MaterialReport;
