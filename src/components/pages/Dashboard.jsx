import React, { useReducer } from "react";
import AdminCardSection1 from "./sections/MediaComponents";
import ChartSection1 from "./sections/DashboardCharts";
import {connect} from 'react-redux'
import { MDBCard, MDBCardBody, MDBCardHeader } from "mdbreact";
import DateRangePicker from "./sections/DateRangePicker";
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
import { 
  fetchCompanyViolatorsData,
  fetchMainCategoriesViolationStatus,
  fetchStreamRecordsStatus,
  fetchProductViolatorsData,
  fetchViolationCauseData
} from '../../actions/companyAction'; 
const _dashboardPage = '5fa7744f-8522-4d03-b731-aff6e4b2044a';

const Dashboard = (props) => {
  props.fetchStreamRecordsStatus();
  props.fetchMainCategoriesViolationStatus();
  props.fetchViolationCauseData();
  props.fetchProductViolatorsData();
  props.fetchCompanyViolatorsData();

  return isAuthorize(_dashboardPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBCard className="mb-5">
              <MDBCardHeader>
                <h1>Dashboard</h1>
              </MDBCardHeader>
              <MDBCardBody>
                 <DateRangePicker /> 
                <br />
                <hr />
                <AdminCardSection1 />
              </MDBCardBody>
              <MDBCardBody style={{ paddingTop: "0px" }}>
                <ChartSection1 />
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};

export default connect(null, {
  fetchCompanyViolatorsData,
  fetchMainCategoriesViolationStatus,
  fetchStreamRecordsStatus,
  fetchProductViolatorsData,
  fetchViolationCauseData
})(Dashboard);
