import React, { useState, useEffect, Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  MDBCard,
  MDBCardBody,
  MDBCardHeader,
  MDBTabPane,
  MDBTabContent,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBNav,
  MDBNavItem,
  MDBNavLink
} from "mdbreact";
import { isAuthorize } from "../utils";
import UnAuthorize from './UnAuthorize'
import { fetchProductMaterialsReportData } from "../../actions/companyAction";
import RadioProductReport from "./RadioProductReport";
import OnlineProductReport from "./OnlineProductReport";
const _companyProductMaterialReportPage = '465a1857-89db-4177-999b-dec2d150b930';

const ProductReport = props => {
  const [activeItem, setActiveItem] = useState("1");
  const [navColor, setNavColor] = useState("blue-text");
  const [history, setHistory] = useState({});
  const [productDetails, setProductDetails] = useState({});
  const [radioMaterialList, setRadioMaterialList] = useState([]);

  const { productId, companyId } = props.match.params;
  if (Object.keys(productDetails).length <= 0)
    props.fetchProductMaterialsReportData(productId);
  if (
    props.productMaterialsReport &&
    props.productMaterialsReport.materialsData
  ) {
    setRadioMaterialList(props.productMaterialsReport.materialsData);
    delete props.productMaterialsReport.materialsData;
    setProductDetails(props.productMaterialsReport);
  }

  const toggle = tab => e => {
    if (activeItem !== tab) {
      setActiveItem(tab);
    }
  };

  useEffect(() => {
    setHistory(props.location.state);
  }, [props.match]);

  return isAuthorize(_companyProductMaterialReportPage) ? (
    <div className="content-wrapper">
      <div className="content">
        <div className="container-fluid">
          <div className="col-md-12">
            <br />
            <MDBBreadcrumb  className={'breadCrumb'}>
              <MDBBreadcrumbItem>
                <Link
                  className={'breadCrumb-link'}
                  to={{ pathname: "/companyreports" }} active
                >
                  COMPANIES
                </Link>
              </MDBBreadcrumbItem>
              <MDBBreadcrumbItem>
                <Link
                  className={'breadCrumb-link'}
                  to={{
                    pathname: `/companyreports/${companyId}`
                  }}
                >
                  {productDetails &&
                    productDetails.companyDetails &&
                    productDetails.companyDetails.name}
                </Link>
              </MDBBreadcrumbItem>
              <MDBBreadcrumbItem>
                <Link
                  className={'breadCrumb-link'}
                  to={{
                    pathname: `/companyreports/${companyId}/productreports/${productDetails.id}`
                  }}
                >
                  {productDetails &&
                    productDetails.companyDetails &&
                    productDetails.name}
                </Link>
              </MDBBreadcrumbItem>
            </MDBBreadcrumb>
            <br />
            <MDBCard className="mb-5">
              <MDBCardHeader>
                <h3>{productDetails && productDetails.name} Report</h3>
              </MDBCardHeader>
              <MDBCardBody>
                <h5>
                  Total Non-Compliant:{" "}
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "30px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "red"
                    }}
                  >
                    {productDetails && productDetails.nonCompliant}
                  </text>
                </h5>
                <h5>
                  Total Potential Non-Compliant:{" "}
                  <text
                    className="mt-2"
                    style={{
                      fontSize: "30px",
                      padding: "5px",
                      fontWeight: "500",
                      color: "orange"
                    }}
                  >
                    {productDetails && productDetails.potentialNonCompliant}
                  </text>
                </h5>
                {/* <MDBContainer className="container-fluid"> */}
                <MDBNav className="nav nav-tabs ml-0 ml-lg-0">
                  <MDBNavItem>
                    <MDBNavLink
                      className={activeItem !== "1" && navColor}
                      to="#"
                      active={activeItem === "1"}
                      onClick={toggle("1")}
                      role="tab"
                      data-toggle="pill"
                    >
                      Radio
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      to="#"
                      className={activeItem !== "2" && navColor}
                      active={activeItem === "2"}
                      onClick={toggle("2")}
                      role="tab"
                      data-toggle="pill"
                    >
                      TV
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      to="#"
                      className={activeItem !== "3" && navColor}
                      active={activeItem === "3"}
                      onClick={toggle("3")}
                      role="tab"
                      data-toggle="pill"
                    >
                      OOH
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      to="#"
                      className={activeItem !== "4" && navColor}
                      active={activeItem === "4"}
                      onClick={toggle("4")}
                      role="tab"
                      data-toggle="pill"
                    >
                      Online
                    </MDBNavLink>
                  </MDBNavItem>
                  <MDBNavItem>
                    <MDBNavLink
                      to="#"
                      className={activeItem !== "5" && navColor}
                      active={activeItem === "5"}
                      onClick={toggle("5")}
                      role="tab"
                      data-toggle="pill"
                    >
                      Print Ads
                    </MDBNavLink>
                  </MDBNavItem>
                </MDBNav>

                <MDBTabContent
                  activeItem={activeItem}
                  style={{
                    padding: "10px"
                  }}
                >
                  <MDBTabPane tabId="1" role="tabpanel">
                    <RadioProductReport materialList={radioMaterialList} />
                  </MDBTabPane>
                  <MDBTabPane tabId="2" role="tabpanel">
                    <p className="mt-2">TV</p>
                  </MDBTabPane>
                  <MDBTabPane tabId="3" role="tabpanel">
                    <p className="mt-2">OOH</p>
                  </MDBTabPane>
                  <MDBTabPane tabId="4" role="tabpanel">
                  <p className="mt-2">Online</p>
                  </MDBTabPane>
                  <MDBTabPane tabId="5" role="tabpanel">
                    <p className="mt-2">Print Ads</p>
                  </MDBTabPane>
                </MDBTabContent>

                {/* </MDBContainer> */}
              </MDBCardBody>
            </MDBCard>
          </div>
          <br />
        </div>
      </div>
    </div>
  ) : <UnAuthorize />;
};

const mapStateToProps = state => ({
  productMaterialsReport: state.data.productMaterialsReport
});

export default connect(mapStateToProps, { fetchProductMaterialsReportData })(
  ProductReport
);
