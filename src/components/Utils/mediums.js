export default [
  {
    id: 1,
    type: "TV"
  },
  {
    id: 2,
    type: "Radio"
  },
  {
    id: 3,
    type: "OOH"
  },
  {
    id: 4,
    type: "Online"
  },
  {
    id: 5,
    type: "Print Ad"
  }
];