const data = [
  {
    name: "Brand 1",
    noOfProducts: "10",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Brand 2",
    noOfProducts: "8",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Brand 3",
    noOfProducts: "7",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Brand 4",
    noOfProducts: "5",
    totalCompliance: "8",
    totalNonCompliance: "2"
  },
  {
    name: "Brand 5",
    noOfProducts: "3",
    totalCompliance: "7",
    totalNonCompliance: "2"
  }
];
export default data;
