const data = [
  {
    name: "ABS-CBN",
    status: "Complied",
    region: "Region VII",
    province: "Cebu City",
    totalCompliance: "5",
    totalNonCompliance: "2",
    airedMaterial: "../pages/audio.mp3"
  },
  {
    name: "GMA",
    status: "Complied",
    region: "Region VII",
    province: "Cebu City",
    totalCompliance: "5",
    totalNonCompliance: "2",
    airedMaterial: "Aired Material should be here"
  },
  {
    name: "TV5",
    status: "Complied",
    region: "Region VII",
    province: "Cebu City",
    totalCompliance: "5",
    totalNonCompliance: "2",
    airedMaterial: "Aired Material should be here"
  },
  {
    name: "Light TV",
    status: "Complied",
    region: "Region VII",
    province: "Cebu City",
    totalCompliance: "5",
    totalNonCompliance: "2",
    airedMaterial: "Aired Material should be here"
  },
  {
    name: "ABS-CBN",
    status: "Non-Complied",
    region: "Region VII",
    province: "Cebu City",
    totalCompliance: "5",
    totalNonCompliance: "2",
    airedMaterial: "Aired Material should be here"
  },
];
export default data;
