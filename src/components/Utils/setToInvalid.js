export default (target) => {
  let currentClassName = target.getAttribute('class');
  if(currentClassName && currentClassName.indexOf('is-invalid') <= -1){
    currentClassName += " is-invalid";
    target.setAttribute('class', currentClassName);
  } 
}
