const data = [
  {
    name: "Product 1",
    noOfProducts: "10",
    mainCategory: "Drinks",
    subCategory: "Alcoholic",
    subSubCategory: "Beer",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Product 2",
    noOfProducts: "8",
    mainCategory: "Drinks",
    subCategory: "Non-Alcoholic",
    subSubCategory: "Milk",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Product 3",
    noOfProducts: "7",
    mainCategory: "Drinks",
    subCategory: "Non-Alcoholic",
    subSubCategory: "Milk",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Product 4",
    noOfProducts: "5",
    mainCategory: "Drinks",
    subCategory: "Non-Alcoholic",
    subSubCategory: "Milk",
    totalCompliance: "8",
    totalNonCompliance: "2"
  },
  {
    name: "Product 5",
    noOfProducts: "3",
    mainCategory: "Drinks",
    subCategory: "Alcoholic",
    subSubCategory: "Beer",
    totalCompliance: "7",
    totalNonCompliance: "2"
  }
];
export default data;
