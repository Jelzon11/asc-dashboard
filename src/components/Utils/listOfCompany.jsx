const data = [
  {
    name: "Unilab",
    noOfBrands: "129",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "P&G",
    noOfBrands: "89",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Jollibee Food Corporation",
    noOfBrands: "85",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "Unilever",
    noOfBrands: "45",
    totalCompliance: "5",
    totalNonCompliance: "2"
  },
  {
    name: "San Miguel",
    noOfBrands: "39",
    totalCompliance: "5",
    totalNonCompliance: "2"
  }
];
export default data;
