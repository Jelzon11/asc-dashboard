import XLSX from 'xlsx'
import { saveAs } from 'file-saver';
import moment from 'moment';

export default ({column, data, subject, workBookTitle}) => {
  const promiseData = new Promise((resolve) => {
    const sheetData = data.map((item) => {
      let record = [];
      for(let i = 0; i < column.length; i++){
        if(column[i].subKey){
          record.push(item[column[i].key][column[i].subKey]);
        } else if(column[i].header == 'DateTime'){
          record.push(moment.unix(item[column[i].key]).format('MM-DD-YYYY hh:mm:ss'));
        } else {
          record.push(item[column[i].key]);
        }        
      }
      return record;
    })
    let finalData = [];
    finalData.push(column.map((i) => i.header));
    finalData = [...finalData, ...sheetData];
    resolve(finalData)
  })
  promiseData.then((data) => {
    var wb = XLSX.utils.book_new();
    wb.Props = {
      Title: workBookTitle,
      Subject: subject,
      Author: "ASC-Report",
      CreatedDate: moment().format('MM DD YYYY hh:mm:ss')
    };
    wb.SheetNames.push(subject);
    var ws = XLSX.utils.aoa_to_sheet(data);
    wb.Sheets[`${subject}`] = ws;
    var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
    function s2ab(s) { 
      var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
      var view = new Uint8Array(buf);  //create uint8array as viewer
      for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
      return buf;    
    }
    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), `${moment().format('MM-DD0-YYYY-hh-mm-ss')}_${subject}.xlsx`);
  })
}

