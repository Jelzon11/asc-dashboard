const data = [
  {
    name: "Material 1",
    commercialType: "Thematic",
    totalCompliance: "5",
    totalNonCompliance: "2",
    originalMaterial: "Video File should be here",
  },
  {
    name: "Material 2",
    commercialType: "Thematic",
    totalCompliance: "5",
    totalNonCompliance: "2",
    originalMaterial: "Video File should be here",
  },
  {
    name: "Material 3",
    commercialType: "Thematic",
    totalCompliance: "5",
    totalNonCompliance: "2",
    originalMaterial: "Video File should be here",
  },
  {
    name: "Material 4",
    commercialType: "Thematic",
    totalCompliance: "5",
    totalNonCompliance: "2",
    originalMaterial: "Video File should be here",
  },
  {
    name: "Material 5",
    commercialType: "Thematic",
    totalCompliance: "5",
    totalNonCompliance: "2",
    originalMaterial: "Video File should be here",
  },
];
export default data;
