import {
  FETCH_COMPANY_REPORTS,
  FETCH_COMPANY_PRODUCTS_REPORTS,
  RESET_COMPANY_PRODUCTS_REPORTS,
  FETCH_PRODUCT_MATERIALS_REPORTS,
  FETCH_MATERIAL_STREAMRECORDS_REPORTS,
  FETCH_VIOLATORS_BY_COMPANY_REPORTS,
  FETCH_VIOLATIONS_REPORTS,
  FETCH_VIOLATORS_BY_PRODUCT_REPORTS,
  FETCH_MAINCATEGORIES_BY_VIOLATIONS_STATUS,
  FETCH_STREAM_RECORD_STATUS,
  FETCH_NON_EXISTING_MATERIAL,
  FETCH_ME,
  FETCH_ALL,
} from "./types";
import { getToken } from "../components/utils";
const url = process.env.REACT_APP_URL;
const token = "bearer " + getToken();

export const fetchCompanyReportData = () => dispatch => {
  fetch(url + "/ex/api/companies/reports", {
    method: "GET",
    headers: { "content-type": "application/json" }
  })
    .then(res => res.json())
    .then(resData => {
      //console.log('resData:', resData);
      dispatch({
        type: FETCH_COMPANY_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchCompanyProductReportData = (
  companyId,
  callback
) => dispatch => {
  fetch(url + "/ex/api/companies/" + companyId + "/products/reports", {
    method: "GET",
    headers: {
      "content-type": "application/x-www-form-urlencoded"
      //'authorization': token
    }
    //body: JSON.stringify(companyId)
  })
    .then(res => res.json())
    .then(resData => {
      dispatch({
        type: FETCH_COMPANY_PRODUCTS_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const resetSelectedCompanyProductReportData = () => dispatch => {
  dispatch({
    type: RESET_COMPANY_PRODUCTS_REPORTS
  });
};

export const fetchProductMaterialsReportData = productId => dispatch => {
  fetch(url + "/ex/api/products/" + productId + "/materials/reports", {
    method: "GET",
    headers: {
      "content-type": "application/x-www-form-urlencoded"
      //'authorization': token
    }
    //body: JSON.stringify(companyId)
  })
    .then(res => res.json())
    .then(resData => {
      //console.log('resData:', resData);
      dispatch({
        type: FETCH_PRODUCT_MATERIALS_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchMaterialStreamRecordsReportData = (
  materialId,
  callback
) => dispatch => {
  fetch(url + "/ex/api/materials/" + materialId + "/streamrecords/reports", {
    method: "GET",
    headers: {
      "content-type": "application/x-www-form-urlencoded"
      //'authorization': token
    }
    //body: JSON.stringify(companyId)
  })
    .then(res => res.json())
    .then(resData => {
      //console.log('resData:', resData);
      dispatch({
        type: FETCH_MATERIAL_STREAMRECORDS_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const updateStatus = (statusData, callback) => dispatch => {
  //console.log(statusData);
  fetch(url + "/ex/api/streamrecords", {
    method: "PUT",
    headers: {
      "content-type": "application/json"
      //authorization: token
    },
    body: JSON.stringify(statusData)
  }).then(res => {
    callback();
  });
};

export const updateReason = (reason, callback) => dispatch => {
  fetch(url + "/ex/api/unidentifieds/materials", {
    method: "PUT",
    headers: {
      "content-type": "application/json"
      //authorization: token
    },
    body: JSON.stringify(reason)
  }).then(res => {
    callback();
  });
};

export const fetchAllData = (startdate, enddate) => dispatch => {
  let queryString = "/ex/api/streamrecords";
  if (startdate && enddate)
    queryString = `/ex/api/streamrecords?startdate=${startdate}&enddate=${enddate}`;
  fetch(url + queryString, {
    method: "GET",
    headers: {
      "content-type": "application/json"
    }
  })
    .then(res => res.json())
    .then(resData => {
      dispatch({
        type: FETCH_ALL,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchCompanyViolatorsData = dateRange => dispatch => {
  let queryString;
  if (dateRange) {
    queryString =
      "/ex/api/violators/companies?startdate=" +
      dateRange.startdate +
      "&enddate=" +
      dateRange.enddate;
  } else {
    queryString = "/ex/api/violators/companies";
  }
  fetch(url + queryString, {
    method: "GET",
    headers: { "content-type": "application/json" }
  })
    .then(res => res.json())
    .then(resData => {
      //console.log('resData:', resData);
      dispatch({
        type: FETCH_VIOLATORS_BY_COMPANY_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchProductViolatorsData = dateRange => dispatch => {
  let queryString;
  if (dateRange) {
    queryString =
      "/ex/api/violators/products?startdate=" +
      dateRange.startdate +
      "&enddate=" +
      dateRange.enddate;
  } else {
    queryString = "/ex/api/violators/products";
  }
  fetch(url + queryString, {
    method: "GET",
    headers: { "content-type": "application/json" }
  })
    .then(res => res.json())
    .then(resData => {
      //console.log('resData:', resData);
      dispatch({
        type: FETCH_VIOLATORS_BY_PRODUCT_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchViolationCauseData = dateRange => dispatch => {
  let queryString;
  if (dateRange) {
    queryString =
      "/ex/api/violationcauses/reports?startdate=" +
      dateRange.startdate +
      "&enddate=" +
      dateRange.enddate;
  } else {
    queryString = "/ex/api/violationcauses/reports";
  }
  fetch(url + queryString, {
    method: "GET",
    headers: { "content-type": "application/json" }
  })
    .then(res => res.json())
    .then(resData => {
      dispatch({
        type: FETCH_VIOLATIONS_REPORTS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchMainCategoriesViolationStatus = dateRange => dispatch => {
  let queryString;
  if (dateRange) {
    queryString =
      "/ex/api/maincategories/violations/status/reports?startdate=" +
      dateRange.startdate +
      "&enddate=" +
      dateRange.enddate;
  } else {
    queryString = "/ex/api/maincategories/violations/status/reports";
  }
  fetch(url + queryString, {
    method: "GET",
    headers: { "content-type": "application/json" }
  })
    .then(res => res.json())
    .then(resData => {
      dispatch({
        type: FETCH_MAINCATEGORIES_BY_VIOLATIONS_STATUS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchStreamRecordsStatus = (dateRange) => dispatch => {
let queryString;
if (dateRange) {
  queryString = "/ex/api/streamrecords/status/reports?startdate=" + dateRange.startdate + "&enddate=" + dateRange.enddate;
 
} else{
  queryString = "/ex/api/streamrecords/status/reports"
 
}
fetch(url +  queryString, {
  method: "GET",
  headers: { "content-type": "application/json" }
})
  .then(res => res.json())
  .then(resData => {
    dispatch({
      type: FETCH_STREAM_RECORD_STATUS,
      payload: resData
    });
  })
    .then(res => res.json())
    .then(resData => {
      //console.log("resData-streamrecords:", resData);
      dispatch({
        type: FETCH_STREAM_RECORD_STATUS,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const fetchNonExistingMaterials = (startdate, enddate) => dispatch => {
  let queryString = "/ex/api/unidentifieds/materials";
  if (startdate && enddate) {
  queryString = `/ex/api/unidentifieds/materials?startdate=${startdate}&enddate=${enddate}`;
  }
  fetch(url + queryString, {
    method: "GET",
    headers: { "content-type": "application/json" }
  })
    .then(res => res.json())
    .then(resData => {
      //console.log('resData:', resData);
      dispatch({
        type: FETCH_NON_EXISTING_MATERIAL,
        payload: resData
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const LogoutUser = callback => {
  fetch(url + "/api/users/logins", {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
      authorization: token
    }
  }).then(() => {
    callback && callback();
  });
};

export const fetchMyInfo = callback => dispatch => {
  fetch(url + "/api/users/me", {
    method: "GET",
    headers: {
      "content-type": "application/json",
      authorization: token
    }
  })
    .then(res => res.json())
    .then(user => {
      dispatch({
        type: FETCH_ME,
        payload: user
      });
      callback && callback({isAuthenticated: true});
    }).catch(() => callback({isAuthenticated: false}))
};
