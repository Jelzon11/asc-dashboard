export const FETCH_DATA = 'FETCH_DATA';
export const FETCH_MAINCATEGORIES_BY_VIOLATIONS_STATUS = 'FETCH_MAINCATEGORIES_BY_VIOLATIONS_STATUS';
export const FETCH_STREAM_RECORD_STATUS = 'FETCH_STREAM_RECORD_STATUS';
export const FETCH_VIOLATORS_BY_COMPANY_REPORTS = 'FETCH_VIOLATORS_BY_COMPANY_REPORTS';
export const FETCH_VIOLATORS_BY_PRODUCT_REPORTS = 'FETCH_VIOLATORS_BY_PRODUCT_REPORTS';
export const FETCH_VIOLATIONS_REPORTS = 'FETCH_VIOLATIONS_REPORTS';
export const UPDATE_STATUS = 'UPDATE_STATUS';

export const FETCH_COMPANY_REPORTS = 'FETCH_COMPANY_REPORTS';
export const FETCH_COMPANY_PRODUCTS_REPORTS = 'FETCH_COMPANY_PRODUCTS_REPORTS';
export const RESET_COMPANY_PRODUCTS_REPORTS = 'RESET_COMPANY_PRODUCTS_REPORTS';
export const FETCH_PRODUCT_MATERIALS_REPORTS = 'FETCH_PRODUCT_MATERIALS_REPORTS';
export const FETCH_MATERIAL_STREAMRECORDS_REPORTS = 'FETCH_MATERIAL_STREAMRECORDS_REPORTS';

export const FETCH_ME = 'FETCH_ME';

export const FETCH_NON_EXISTING_MATERIAL = 'FETCH_NON_EXISTING_MATERIAL';

export const FETCH_ALL = 'FETCH_ALL';

